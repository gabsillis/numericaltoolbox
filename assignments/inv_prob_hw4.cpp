#include "Numtool/Errors.h"
#include "newtonCG.hpp"
#include <fstream>
#include <iostream>
using T = double;
using IDX = int;


using namespace OPTIMIZATION;
using namespace MATH::TENSOR;
T cost1(T *x){
    return x[0] * x[0] + x[1] * x[1] + x[0] * x[1] - 3 * x[0];
}

void grad1(T *x, T *g){
    g[0] = 2 * x[0] + x[1] - 3;
    g[1] = 2 * x[1] + x[0];
}

void hess_apply1(T *x, T *v, T *out){
    out[0] = 2 * v[0] + 1 * v[1];
    out[1] = 1 * v[0] + 2 * v[1];
}

Matrix<T, IDX> A = {
 { 5.0, 1.0, 0.0, 0.5 },
 { 1.0, 4.0, 0.5, 0.0 },
 { 0.0, 0.5, 3.0, 0.0 },
 { 0.5, 0.0, 0.0, 2.0 }
};
static IDX n = 4;

/** @brief cost function for problem 1 of homework 4 */
struct costp1 {
    private:
    T sigma;
    T mu;
    Matrix<T, IDX> IpmuA;
    public: 
    costp1(T sigma, T mu) : sigma(sigma), mu(mu), IpmuA(A) {
        IpmuA *= mu;
        for(int j = 0; j < n; ++j) IpmuA[j][j] += 1;
    }

    T operator()(T *x){
        T xAx = A.matInnerProd(x);
        return 0.5 * IpmuA.matInnerProd(x) + sigma * 0.25 * SQUARED(xAx);
    }
};

/** @brief gradient of cost function for problem 1 of homework 4 */
struct gradp1 {
    private:
    T sigma;
    T mu;
    Matrix<T, IDX> IpmuA;
    public: 
    gradp1(T sigma, T mu) : sigma(sigma), mu(mu), IpmuA(A) {
        IpmuA *= mu;
        for(int j = 0; j < n; ++j) IpmuA[j][j] += 1;
    }

    void operator()(T *x, T *g){
        T xAx = A.matInnerProd(x);
        Vector<T, IDX> temp(n);
        temp.zeroFill();
        A.mult(x, temp.data);
        temp *= (sigma * xAx);
        IpmuA.mult(x, g);
        for(int j = 0; j < n; ++j) g[j] += temp[j];

        /*
        std::cout << "x = [" << x[0];
        for(int i = 1; i < 4; ++i) std::cout << ", " << x[i];
        std::cout << "]\n";
        std::cout << "gfcn = [" << g[0];
        for(int i = 1; i < 4; ++i) std::cout << ", " << g[i];
        std::cout << "]\n";
        */
    }
};

/** @brief structure that calculates the gradient of a cost function through finite differences */
template<typename cost_functional>
struct finite_difference_gradient{
    cost_functional *costptr;
    IDX n;
    static constexpr T epsilon = 1e-8;
    finite_difference_gradient(cost_functional &cost, IDX n) : costptr(&cost), n(n) {}

    void operator()(T *x, T *g){
        cost_functional &cost = *costptr;
        T costbase = cost(x);
        for(int i = 0; i < n; ++i){
            T temp = x[i];
            x[i] += epsilon;
            T cost_peturb = cost(x);
            x[i] = temp;
            g[i] = (cost_peturb - costbase) / epsilon;
        }
    }
};

/** @brief hessian cost function application to a vector for problem 1 of homework 4 */
struct hess_applyp1 {
    private:
    T sigma;
    T mu;
    Matrix<T, IDX> IpmuA;
    public: 
    hess_applyp1(T sigma, T mu) : sigma(sigma), mu(mu), IpmuA(A) {
        IpmuA *= mu;
        for(int j = 0; j < n; ++j) IpmuA[j][j] += 1;
    }

    void operator()(T *x, T *v, T *out){
        Vector<T, IDX> outvec(out, n);
        Vector<T, IDX> vvec(v, n);
        Vector<T, IDX> xvec(x, n);
        Vector<T, IDX> term1(n);
        Vector<T, IDX> term2(n);
        Vector<T, IDX> Ax(n);
        // calculate Ax
        A.mult(x, Ax.data);

        // calculate term 1
        IpmuA.mult(v, term1.data);

        // calculate term2
        term2 = Ax;
        term2 *= (2 * sigma * Ax.dot(vvec));
        
        // calculate final term
        A.mult(v, out);
        outvec *= (sigma * xvec.dot(Ax));

        // add terms
        outvec += term1;
        outvec += term2;
    }
};

void printDiagnostics(NewtonCGDiagnostics<T, IDX> &diag, std::ostream &out){
    out << diag.grad_norms[0] << " 0" << std::endl;
    for(IDX i = 0; i < diag.n_nl_iterations; ++i){
        out << diag.grad_norms[i+1] << " " << diag.linesearch_alpha[i] << std::endl;
    }
}

inline T sind(T x){ return sin(x * M_PI / 180.0); }
inline T cosd(T x){ return cos(x * M_PI / 180.0); }

int main(){
    std::cout << "\n Homework 4-1:\n";
    // create a newton CG solver
    NewtonCG<T, IDX> newtonsolver{};
    NewtonCGDiagnostics<T, IDX> diag;

    // create x, and define an initialize function
    Vector<T, IDX> x(n);
    auto init_x = [&](){
        x[0] = cosd(70);
        x[1] = sind(70);
        x[2] = cosd(70);
        x[3] = sind(70);
    };

    // Define the cost, gradient and hessian for both parameter sets
    costp1 cost(1.0, 0.0);
    gradp1 grad(1.0, 0.0);
    hess_applyp1 hess(1.0, 0.0);

    costp1 costb(1.0, 10.0);
    gradp1 gradb(1.0, 10.0);
    hess_applyp1 hessb(1.0, 10.0);

    // lambda function for identity to use steepest descent
    // (a little cheeky but it works)
    auto steepest_hess = [](T *x, T *v, T *out){
        for(int i = 0; i < 4; ++i) out[i] = v[i];
    };

    // optional finite difference gradient
    finite_difference_gradient<costp1> fd_grad(cost, n);

    // set the maximum nonlinear iterations
    newtonsolver.nl_it_max = 200;

    std::ofstream out;
    // ============== Coefficient Set A ===============
    // eta = 0.5, full hessian
    init_x();
    newtonsolver.eta_strategy = NewtonCG<T, IDX>::HALF;
    diag = newtonsolver.solve(cost, grad, hess, n, x, false);
    out = std::ofstream("output/paramA_etahalf_hess.dat");
    printDiagnostics(diag, out);

    // steepest descent
    init_x();
    diag = newtonsolver.solve(cost, grad, steepest_hess, n, x, false);
    out = std::ofstream("output/paramA_etahalf_steepest.dat");
    printDiagnostics(diag, out);

    // eta = sqrt grad norm, full hessian
    init_x();
    newtonsolver.eta_strategy = NewtonCG<T, IDX>::SQRT_GRADIENT_NORM;
    diag = newtonsolver.solve(cost, grad, hess, n, x, false);
    out = std::ofstream("output/paramA_etasqrtg_hess.dat");
    printDiagnostics(diag, out);

    // steepest descent
    init_x();
    diag = newtonsolver.solve(cost, grad, steepest_hess, n, x, false);
    out = std::ofstream("output/paramA_etasqrtg_steepest.dat");
    printDiagnostics(diag, out);

    // eta = grad norm, full hessian
    init_x();
    newtonsolver.eta_strategy = NewtonCG<T, IDX>::GRADIENT_NORM;
    diag = newtonsolver.solve(cost, grad, hess, n, x, false);
    out = std::ofstream("output/paramA_etag_hess.dat");
    printDiagnostics(diag, out);

    // steepest descent
    init_x();
    diag = newtonsolver.solve(cost, grad, steepest_hess, n, x, false);
    out = std::ofstream("output/paramA_etag_steepest.dat");
    printDiagnostics(diag, out);


    // ============== Coefficient Set B ===============
    // eta = 0.5, full hessian
    init_x();
    newtonsolver.eta_strategy = NewtonCG<T, IDX>::HALF;
    diag = newtonsolver.solve(costb, gradb, hessb, n, x, false);
    out = std::ofstream("output/paramB_etahalf_hess.dat");
    printDiagnostics(diag, out);

    // steepest descent
    init_x();
    diag = newtonsolver.solve(costb, gradb, steepest_hess, n, x, false);
    out = std::ofstream("output/paramB_etahalf_steepest.dat");
    printDiagnostics(diag, out);

    // eta = sqrt gradb norm, full hessian
    init_x();
    newtonsolver.eta_strategy = NewtonCG<T, IDX>::SQRT_GRADIENT_NORM;
    diag = newtonsolver.solve(costb, gradb, hessb, n, x, false);
    out = std::ofstream("output/paramB_etasqrtg_hess.dat");
    printDiagnostics(diag, out);

    // steepest descent
    init_x();
    diag = newtonsolver.solve(costb, gradb, steepest_hess, n, x, false);
    out = std::ofstream("output/paramB_etasqrtg_steepest.dat");
    printDiagnostics(diag, out);

    // eta = gradb norm, full hessian
    init_x();
    newtonsolver.eta_strategy = NewtonCG<T, IDX>::GRADIENT_NORM;
    diag = newtonsolver.solve(costb, gradb, hessb, n, x, true);
    out = std::ofstream("output/paramB_etag_hess.dat");
    printDiagnostics(diag, out);

    // steepest descent
    init_x();
    diag = newtonsolver.solve(costb, gradb, steepest_hess, n, x, false);
    out = std::ofstream("output/paramB_etag_steepest.dat");
    printDiagnostics(diag, out);
}

/**
 * @file GNPetsc.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief A petsc implementation of the Gauss-Newton algorithm
 * @version 0.1
 * @date 2022-07-29
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "optimization.hpp"
#include <petscksp.h>
namespace OPTIMIZATION {

    namespace GN_OPTIONS {
        enum CONVERGENCE_TEST{
            RESIDUAL_TO_ZERO
        };
    }
    template<typename T, typename IDX, GN_OPTIONS::CONVERGENCE_TEST conv_test = GN_OPTIONS::RESIDUAL_TO_ZERO>
    class GaussNewtonPetsc {
        private:
        static constexpr T LAMBDA_DEFAULT{1e-8};
        public:

        KSP ksp;
        /// Absolute Tolerance (for residual based convergence only)
        T abs_tol = 1e-8;
        /// Relative Tolerance
        T relative_tol = 1e-8;

        GaussNewtonPetsc(){
            
        }

        void solve(Vec x, Vec lambdaI, CostFunctionPetsc<T, IDX> &cost, IDX kmax = 100){
            Mat J, JTJ;
            Vec r, JTr, p, diag;

            // Initialize ksp
            KSPCreate(PETSC_COMM_WORLD, &ksp); 

            // Initialize Vectors
            VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, cost.getN(), &p);
            VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, cost.getN(), &diag);
            VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, cost.getN(), &JTr);
            VecCreateSeq(PETSC_COMM_SELF, cost.getM(), &r);

            // Initialize Matrices
            MatCreateAIJ(PETSC_COMM_WORLD, 
                PETSC_DECIDE, PETSC_DECIDE, cost.getM(), cost.getN(),
                0, NULL, 0, NULL, &J
            );
            MatSetOption(J, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);
            MatSetUp(J);

            // get the initial residual
            cost.residual(x, r);
            PetscReal r0norm, r0normAll, rknorm, rknormAll;
            VecNorm(r, NORM_2, &r0norm);
            r0norm = SQUARED(r0norm);
            MPI_Allreduce(&r0norm, &r0normAll, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
            r0normAll = std::sqrt(r0normAll);
            PetscReal tau = abs_tol + relative_tol * r0normAll; 
            for(int k = 0; k < kmax; k++){
                // Get the residual and Jacobian
                cost.formResidualAndJacobian(x, r, J);

                // Convergence tests`
                if constexpr(conv_test == GN_OPTIONS::RESIDUAL_TO_ZERO){
                    VecNorm(r, NORM_2, &rknorm);
                    rknorm = SQUARED(rknorm);
                    MPI_Allreduce(&rknorm, &rknormAll, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
                    rknormAll = std::sqrt(rknormAll);
                    PetscPrintf(PETSC_COMM_WORLD, "Residual value: %lf\n", rknormAll);
                   if(rknormAll < tau){
                       break;
                   } 
                }


                // form JTr
                MatMultTranspose(J, r, JTr);
                VecScale(JTr, -1.0);
                // form JTJ
                MatProductCreate(J, J, NULL, &JTJ);
                MatProductSetType(JTJ, MATPRODUCT_AtB);
                MatProductSetFill(JTJ, PETSC_DEFAULT);

                MatType mtype;
                MatGetType(J, &mtype);
                MatSetType(JTJ, mtype);
                MatSetOption(JTJ, MAT_FORCE_DIAGONAL_ENTRIES, PETSC_TRUE);
                MatProductSetFromOptions(JTJ);
                MatProductSymbolic(JTJ);
                MatSetOption(JTJ, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);
                MatProductNumeric(JTJ);
                // add Regularization to diagonal
                MatGetDiagonal(JTJ, diag);
                VecAXPY(diag, 1.0, lambdaI);
                MatDiagonalSet(JTJ, diag, INSERT_VALUES);

                // solve linear system
                KSPSetOperators(ksp, JTJ, JTJ);
                KSPSetFromOptions(ksp);
                KSPSolve(ksp, JTr, p);

                // Update x
                VecAXPY(x, 1.0, p);
                // Note: if we could garuantee J has same structure every it
                // the JTJ can be formed with MAT_REUSE_MATRIX and would only need to be destroyed at
                // the end
                MatDestroy(&JTJ);
                KSPDestroy(&ksp);
            }
        }
    };
}
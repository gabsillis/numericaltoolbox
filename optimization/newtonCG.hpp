/**
 * @file newtonCG.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Newton CG nonlinear optimization solver
 */
#include "Numtool/Errors.h"
#include "Numtool/matrix/matrix.hpp"
#include <Numtool/tensor.hpp>
#include "linesearch.hpp"
#include <iostream>
#include <iomanip>

namespace OPTIMIZATION {

/**
 * @brief structure for storing diagnostic information
 * - gradient (residual) norms
 * - number of iterations
 * - linesearch parameter
 */
template<typename T, typename IDX>
struct NewtonCGDiagnostics{
    std::vector<T> grad_norms;
    std::vector<T> linesearch_alpha;
    IDX n_nl_iterations = 0;
};

/**
 * @brief Solver that uses NewtonCG to solve a nonlinear system
 * @tparam T the floating point type
 * @tparam IDX the indexing type
 */
template<typename T, typename IDX>
class NewtonCG{
    public:
    enum LINESEARCH_STRATEGY {
        /// Wolfe conditions from Nocedal & Wright using cubic interpolation
        WOLFE
    };
    
    enum TERMINATION_ETA {
        /// ETA = 0.5
        HALF,
        /// ETA = min(0.5, sqrt(norm(gradient)))
        SQRT_GRADIENT_NORM,
        /// ETA = min(0.5, norm(gradient))
        GRADIENT_NORM 
    };

    ///
    IDX nl_it_max = 100;

    /// convergence magnitude reduction for gradient for nonlinear iterations
    T nl_eps = 1e-8;

    /// multiplier by n to set max
    T cg_it_mult = 1;

    /// multiplier by n
    T linesearch_it_mult = 1;

    /// maximum linesearch multiplier
    T alpha_ls_max = 10;

    /// wolfe condition constant one
    T c1 = 1e-4;

    /// wolfe condition constant two
    T c2 = 0.9;

    LINESEARCH_STRATEGY ls_strategy = WOLFE;
    TERMINATION_ETA eta_strategy = HALF;

    template<typename cost_function_T, typename gradient_function_T, typename hessian_apply_function_T>
    NewtonCGDiagnostics<T, IDX> solve(
            cost_function_T &cost,
            gradient_function_T &grad,
            hessian_apply_function_T &hess_apply,
            IDX n,
            MATH::TENSOR::Vector<T, IDX> &x,
            bool verbose_mode = false
    ){
        using namespace MATH::TENSOR;
        Vector<T, IDX> rj(n);
        Vector<T, IDX> gk(n);
        Vector<T, IDX> dj(n);
        Vector<T, IDX> pk(n);
        Vector<T, IDX> z(n);
        Vector<T, IDX> Bd(n);
        Vector<T, IDX> step(n);
        NewtonCGDiagnostics<T, IDX> diag{};
        IDX cg_it_max = (IDX) (cg_it_mult * n);
        IDX linesearch_it_max = (IDX) (linesearch_it_mult * n);
        T dBd, alpha_j, beta_j, eta_k, epsilon_k; // p_j^T B_k p_j where B_k is the hessian or approximation thereof

        // get initial gradient
        grad((T *) x.data, (T *) gk.data);
        T gnorm0 = gk.norm();
        diag.grad_norms.push_back(gnorm0);

        for(IDX k = 0; k < nl_it_max; ++k){
            // initialize cg direction
            z.zeroFill();

            rj = gk;

            // initial guess for step direction (steepest descent)
            dj = -rj;

            if(eta_strategy == HALF){
                eta_k = 0.5;
            } else if (eta_strategy == SQRT_GRADIENT_NORM){
                eta_k = std::min(0.5, sqrt(rj.norm()));
            } else if (eta_strategy == GRADIENT_NORM){
                eta_k = std::min(0.5, rj.norm());
            } else {
                ERR::throwError("No termination strategy selected.");
                return diag;
            }
            epsilon_k = eta_k * rj.norm();
            for(IDX j = 0; j < cg_it_max; ++j){
                // apply hessian B(x) to vector p and store result in Bd
                hess_apply((T *) x.data, (T *) dj.data, (T *) Bd.data);

                dBd = dj.dot(Bd);

                // Hessian positivity condition
                if(dBd <= 0){
                    if(j == 0){
                        pk = dj;
                    } else {
                        pk = z;
                    }
                    break; // p_k = -g_k for first iteration or z_j otherwise
                }

                T rjnorm2 = rj.dot(rj);
                alpha_j = rjnorm2 / dBd;
                z.addScaled(dj, alpha_j);
                rj.addScaled(Bd, alpha_j);
                T rjp1norm2 = rj.dot(rj);

                // convergence criteria
                if(std::sqrt(rjp1norm2) < epsilon_k){
                    pk = z;
                    break;
                }
                beta_j = rjp1norm2 / rjnorm2;
                // update direction
                dj *= beta_j;
                dj.addScaled(rj, -1.0);
            }

            // search direction found from cg, now linesearch with starting alpha of one
            T alpha_ls;
            // reframe the cost function as a scalar function of alpha
            auto linesearch_cost = [&cost, &x, &pk, &step](T alpha_arg){
                step = x;
                step.addScaled(pk, alpha_arg);
                return cost((T *) step.data);
            };

            if(ls_strategy == WOLFE){
                alpha_ls = LINESEARCH::wolfe_linesearch<T>(linesearch_cost, alpha_ls_max, 1.0, linesearch_it_max, c1, c2);
            }
            diag.linesearch_alpha.push_back(alpha_ls);
            // add the step direction times linesearch multiplier
            x.addScaled(pk, alpha_ls);

            // get the new gradient
            grad((T *) x.data, (T *) gk.data);
            T gknorm = gk.norm();
            diag.grad_norms.push_back(gknorm);

            // verbose diagnostic information
            if(verbose_mode){
                std::cout << "Iteration " << std::setw(5) << k << "| alpha: " << std::setw(16) 
                    << alpha_ls << " | gradient norm: " 
                    << std::setw(16) << gk.norm() << "\n";
            }

            if(gknorm < nl_eps * gnorm0){
                std::cout << "Newton CG converged in " << (k + 1) << " iterations.\n";
                diag.n_nl_iterations = k + 1;
                return diag;
            }
        }
        std::cout << "Maximum nonlinear iterations reached in Newton CG.\n";
        diag.n_nl_iterations = nl_it_max;
        return diag;

    }
};

} // end namespace

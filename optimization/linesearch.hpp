/**
 * @file linesearch.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief lineasearch
 * @version 0.1
 * @date 2022-11-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once
#include <cmath>
#include <functional>
#include <limits>
namespace OPTIMIZATION::LINESEARCH {
    
    template<typename T>
    using function1d = std::function<T(T)>;

    template<typename T>
    void find_bracket_aux(function1d<T> fcn, T xa, T xb, T fa, T fb, T fc, T epsilon, T mult, T &a, T &b){
        a = xa;
        if(fa < fc && fc < fb) b = xb;
        else find_bracket_aux<T>(fcn, xa, xb + mult * epsilon, fa, fcn(xb + mult * epsilon), fb, epsilon, 2 * mult, a, b);
    }

    template<typename T>
    void find_bracket(function1d<T> fcn, T x0, T epsilon, T &a, T &b){
        a = x0;
        T x1 = x0 + epsilon;
        T x2 = x1 + 2 * epsilon;
        T fa = fcn(a);
        T fc = fcn(x1);
        T fb = fcn(x2);
        find_bracket_aux<T>(fcn, x0, x2, fa, fb, fc, epsilon, 4, a, b);
    }

    template<typename T>
    T golden_section(function1d<T> fcn, T x0, int N){
        static constexpr T rho = (3 - std::sqrt(5.0)) * 0.5;
        static constexpr T EPSILON_BRACKET = 1e-9;
        T s, t, f1, f2, a, b;
        find_bracket(fcn, x0, EPSILON_BRACKET, a, b);
        s = a + rho * (b - a);
        t = a + (1 - rho) * (b - a);
        f1 = fcn(s);
        f2 = fcn(t);
        for(int k = 0; k < N; k++){
            if(f1 < f2){
                b = t;
                t = s;
                s = a + rho * (b - a);
                f2 = f1;
                f1 = fcn(s);
            } else {
                a = s;
                s = t;
                t = a + (1 - rho) * (b - a);
                f1 = f2;
                f2 = fcn(t);
            }
        }
        return 0.5 * (s + t);
    }

    template<typename T>
    inline T finite_difference(function1d<T> fcn, T EPSILON, T x){
        T phi1 = fcn(x);
        T phi2 = fcn(x + EPSILON);
        return (phi2 - phi1) / EPSILON;
    }

    template<typename T>
    T zoom(function1d<T> fcn, T alpha_lo, T alpha_hi, int kmax, T c1, T c2){
        static constexpr T EPSILON = std::sqrt(std::numeric_limits<T>::epsilon());

        for(int k = 0; k < kmax; k++){
            // cubic interpolation
            T phi_alo = fcn(alpha_lo);
            T dphi_alo = finite_difference(fcn, EPSILON, alpha_lo);
            T phi_ahi = fcn(alpha_hi);
            T dphi_ahi = finite_difference(fcn, EPSILON, alpha_hi);

            T d1 = dphi_alo + dphi_ahi - 3 * (phi_alo - phi_ahi) / (alpha_lo - alpha_hi);
            T d2 = std::sqrt(std::max(SQUARED(d1) - dphi_alo * dphi_ahi, 1e-8)); // safeguard squareroot
            d2 = std::copysign(d2, (alpha_hi - alpha_lo));

            T aj = alpha_hi - (alpha_hi - alpha_lo) * (
                (dphi_ahi + d2 - d1) / 
                (dphi_ahi - dphi_alo + 2 * d2)
            );

            T phi_aj = fcn(aj);
            T dphi_0 = finite_difference(fcn, EPSILON, 0.0);
            if(
                phi_aj > (fcn(0) + c1 * aj * dphi_0)
                || phi_aj >= phi_alo
            ){
                alpha_hi = aj;
            } else {
                T dphi_aj = finite_difference(fcn, EPSILON, aj);
                if( std::abs(dphi_aj <= -c2 * dphi_0) ){
                    return aj;
                }

                if(dphi_aj * (alpha_hi - alpha_lo) >= 0){
                    alpha_hi = alpha_lo;
                }
                alpha_lo = aj;
            }
        }

        // if zoom doesn't finish, average the remaining range
        return 0.5 * (alpha_lo + alpha_hi);
    }

    /**
     * @brief Perform a linesearch based on the Wolfe Conditions
     * see Nocedal, Wright
     * 
     * @tparam T the floating point type
     * @param fcn the 1d function in terms of alpha
     * @param alpha_max the maximum alpha
     * @param alpha1 the initial alpha (reccomend 1 for Newton or scaled for CG)
     * @param kmax the maximum iterations for the linesearch
     * @param c1 the first linesearch constant (reccommend 1e-4)
     * @param c2 the second linesearch constant (reccomend 0.9)
     * @return T the step length alpha that provides sufficient decrease based on the Wolfe Conditions
     */
    template<typename T>
    T wolfe_linesearch(function1d<T> fcn, T alpha_max, T alpha1, int kmax, T c1, T c2){
        static T EPSILON = std::sqrt(std::numeric_limits<T>::epsilon());
        T a_im1 = 0;
        T a_i = alpha1;
        T phi_0 = fcn(0);
        T dphi_0 = finite_difference(fcn, EPSILON, 0.0);
        for(int k = 0; k < kmax; k++){
            T phi_i = fcn(a_i);
            T phi_im1 = fcn(a_im1);
            if(
                phi_i > phi_0 + c1 * a_i * dphi_0
                || (phi_i >= phi_im1 && k > 0)
            ){
                return zoom(fcn, a_im1, a_i, kmax, c1, c2);
            }
            T dphi_i = finite_difference(fcn, EPSILON, a_i);

            if(std::abs(dphi_i) <= -c2 * dphi_0) {
                return a_i;
            }

            if(dphi_i >= 0){
                return zoom(fcn, a_i, a_im1, kmax, c1, c2);
            }

            T temp = 0.5 * (a_i + alpha_max); // compute the new a_i as average of current a_i and alpha_max
            a_im1 = a_i;
            a_i = temp;
        }
        return a_i;
    }
}
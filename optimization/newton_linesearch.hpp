/**
 * @file newton_linesearch.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Newton Linesearch Algorithm
 * @version 0.1
 * @date 2022-11-23
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
#include "optimization.hpp"
#include "linesearch.hpp"
#include <Numtool/tensor.hpp>
#include <Numtool/tensorUtils.hpp>

namespace OPTIMIZATION {


    template<typename T, typename IDX>
    class GN_LinearSubproblem{
        private:
        MATH::TENSOR::Matrix<T, IDX> *jac;
        T lambda = 0; // regularization parameter

        public:
        IDX getM() const {return jac->getN(); }

        IDX getN() const {return jac->getN(); }

        void setJac( MATH::TENSOR::Matrix<T, IDX> &jac_){
            jac = &jac_;
        }

        void setLambda(T lambda_arg){lambda = lambda_arg;}

        void mult(const T *x, T *out) const {
            jac->ATAx(x, out);
            if(lambda != 0){
                for(IDX i = 0; i < jac->getN(); i++) out[i] += lambda * x[i];
            }
        }
    };

    /**
     * @brief Gauss-Newton Linesearch
     * uses the newtaon step J^TJp = -J^Tr
     * 
     * @tparam T the floating point type
     * @tparam IDX the index type
     */
    template<typename T, typename IDX>
    class GaussNewtonLinesearch {
        public: 
        // SOLVER OPTIONS
        IDX kmax = 20;
        T rtol = 1e-8; // relative tolerance for convergence, applied to norm of J^Tr
        T atol = 1e-8; // absolute tolerance for convergence, applied to norm of J^Tr
        T linear_epsilon = 1e-8; // tolerance for linear iterative solver
        T linear_kmax_nmult = 1.0; // multipler by n for the maximum linear iterations (integer truncated)
        T c1 = 1e-4; // first wolfe LS constant
        T c2 = 0.9; // second wolfe LS constant
        T alpha_max = 10; //max linesearch multiplier
        T alpha_initial = 1; // initial linesearch multiplier
        T subproblem_lambda = 0; // regularization parameter for linear subproblem
        IDX ls_kmax = 20; // maximum iterations for inner linesearch

        /**
         * @brief Minimize the cost function
         * 
         * @param [in] cost the cost/objective function
         * @param [in, out] x the starting guess, result of optimization stored here
         * @param [out] residuals vector of iteration residuals
         * @return T the final linesearch multiplier
         */
        T solve(CostFunction<T, IDX> &cost, MATH::TENSOR::Vector<T, IDX> &x, std::vector<T> &residuals){
            using namespace MATH::TENSOR;
            IDX n = cost.getN();
            IDX m = cost.getM();
            Vector<T, IDX> p(n);
            Vector<T, IDX> JTr(n);
            Vector<T, IDX> r(m);
            Vector<T, IDX> workr(m);
            Vector<T, IDX> workx(n);
            Matrix<T, IDX> J(m, n);
            GN_LinearSubproblem<T, IDX> subproblem{};
            T alpha;
            p.zeroFill();

            subproblem.setLambda(subproblem_lambda);

            cost.eval(x.data, r.data);
            T normr0 = r.norm();
            residuals.push_back(normr0);

            cost.jacobianDense(x.data, J);
            JTr.zeroFill();
            J.gemvT(-1.0, r.data, 0.0, JTr.data); // -JTr

            J.TransposeMultiply(r, workx);
            T JTrnorm0 = JTr.norm();
            T tau = atol + JTrnorm0 * rtol;

            // set the subproblem jacobian (assume size stays constant within linesearch algorithm)
            subproblem.setJac(J);

            for(IDX k = 0; k < kmax; k++){

                // check for convergence
                if(JTr.norm() < tau) {
                    break;
                }

                // calculate step direction
                KRYLOV::bicgstab<T, IDX, GN_LinearSubproblem<T, IDX>>(subproblem, p.data, JTr.data, linear_epsilon, std::max(10, (IDX) linear_kmax_nmult * n));

                // linesearch
                alpha = LINESEARCH::wolfe_linesearch<T>(
                    [&cost, &x, &p, &workr, &workx, n, m](T alpha){
                        static constexpr T BIG_RESIDUAL = 1e9;
                        workx = x;
                        workx.addScaled(p, alpha);
                        cost.eval(workx.data, workr.data);
                        // safeguard the cost function for linesearch
                        T rnorm = workr.norm();
                        if(std::isfinite(rnorm))
                            return workr.norm();
                        else
                            return BIG_RESIDUAL;
                    }, alpha_max, alpha_initial, ls_kmax, c1, c2
                );

                if(!std::isfinite(alpha)) alpha = 0.5 * alpha_initial;

                // update x
                x.addScaled(p, alpha);

                // update residual and jacobian
                cost.eval(x.data, r.data);
                residuals.push_back(r.norm());

                cost.jacobianDense(x.data, J);
                J.gemvT(-1, r.data, 0.0, JTr.data);
            }
            return alpha; // return the last linesearch multiplier
        }

    };

}
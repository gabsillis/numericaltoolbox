/**
 * @file cgls.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief nonlinear conjugate gradient on a least squares problem
 * @version 0.1
 * @date 2022-11-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "optimization.hpp"
#include <Numtool/tensorUtils.hpp>
#include <Numtool/tensor.hpp>
#include <iostream>
#include "linesearch.hpp"
namespace OPTIMIZATION {

    template<typename T, typename IDX>
    class CGLS {
        private:
        // ---------------------
        // - Solver Parameters -
        // ---------------------
        T tau_abs = 1e-8;
        T tau_rel = 1e-8;
        IDX kmax = 100;


        public:

        void setkmax(IDX kmax_arg){
            kmax = kmax_arg;
        }

        void solve(CostFunction<T, IDX> &cost, MATH::TENSOR::Vector<T, IDX> &x, std::vector<T> &residuals){
            IDX n = cost.getN();
            CostFunctionLS<T, IDX> lscost{cost};
            MATH::TENSOR::Matrix<T, IDX> Q(n, n);
            MATH::TENSOR::Vector<T, IDX> g(n);
            MATH::TENSOR::Vector<T, IDX> d(n);
            MATH::TENSOR::Vector<T, IDX> work1(n); // work array
            lscost.gradient(x.data, g.data);
            for(IDX i = 0; i < n; i++) d[i] = -g[i];

            T gnorm0 = g.norm();
            for(IDX k = 0; k < kmax; k++){
                residuals.push_back(lscost.eval(x.data));
                // reset direction after n+1 steps
                if(k % (n + 1) == 0){ 
                    for(IDX i = 0; i < n; i++) d[i] = -g[i];
                }

                // convergence check
                if(g.norm() < tau_abs + gnorm0 * tau_rel){
                    return;
                }

                // find alpha and update x
                lscost.hess(x.data, Q);
                Q.mult(d.data, work1.data);
                T dQd = d.dot(work1);
                T alpha = -d.dot(g) / dQd;

                // update x
                x.addScaled(d, alpha);

                // find beta and update direction
                lscost.gradient(x.data, g.data);
                // note work1 still contains Q * d
                T beta = g.dot(work1) / dQd;
                for(int i = 0; i < n; i++) d[i] = -g[i] + beta * d[i];
            }
            residuals.push_back(lscost.eval(x.data));
        }
    };

    template<typename T, typename IDX>
    class CGLS_FR { // Flether-Reeves
        private:
        // ---------------------
        // - Solver Parameters -
        // ---------------------
        T tau_abs = 1e-8;
        T tau_rel = 1e-8;
        IDX kmax = 100;
        T alpha_max = 10.0;
        T alpha_initial = 1.0;
        IDX ls_kmax = 20;
        T c1 = 1e-4;
        T c2 = 0.9;


        public:

        void setkmax(IDX kmax_arg){
            kmax = kmax_arg;
        }

        void solve(CostFunction<T, IDX> &cost, MATH::TENSOR::Vector<T, IDX> &x, std::vector<T> &residuals){
            IDX n = cost.getN();
            CostFunctionLS<T, IDX> lscost{cost};
            MATH::TENSOR::Matrix<T, IDX> Q(n, n);
            MATH::TENSOR::Vector<T, IDX> g(n);
            MATH::TENSOR::Vector<T, IDX> gold(n);
            MATH::TENSOR::Vector<T, IDX> d(n);
            MATH::TENSOR::Vector<T, IDX> work1(n); // work array
            lscost.gradient(x.data, g.data);
            gold = g;
            for(IDX i = 0; i < n; i++) d[i] = -g[i];

            T gnorm0 = g.norm();
            for(IDX k = 0; k < kmax; k++){
                residuals.push_back(lscost.eval(x.data));
                // reset direction after n+1 steps
                if(k % (n + 1) == 0){ 
                    for(IDX i = 0; i < n; i++) d[i] = -g[i];
                }

                // convergence check
                if(g.norm() < tau_abs + gnorm0 * tau_rel){
                    return;
                }

//                T alpha = LINESEARCH::golden_section<T>(
//                    [&x, &d, &work1, &lscost](T alpha){
//                        work1 = x;
//                        work1.addScaled(d, alpha);
//                       return lscost.eval(work1.data); // hack in an alpha max
//                    }, 0, 5
//                );



                T alpha = LINESEARCH::wolfe_linesearch<T>(
                    [&x, &d, &work1, &lscost](T alpha){
                        work1 = x;
                        work1.addScaled(d, alpha);
                        return lscost.eval(work1.data); 
                    }, alpha_max, alpha_initial, ls_kmax, c1, c2 
                );

                // update x
                x.addScaled(d, alpha);

                // find beta and update direction
                lscost.gradient(x.data, g.data);
                // Fletcher-Reeves
                T beta = g.dot(g) / gold.dot(gold);
                for(int i = 0; i < n; i++) d[i] = -g[i] + beta * d[i];
            }
            residuals.push_back(lscost.eval(x.data));
        }
    };
}
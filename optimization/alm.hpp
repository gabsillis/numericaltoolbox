/**
 * @file alm.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Self-Adaptive Levenberg Marquardt (Fan, Pan 2006)
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "optimization.hpp"
#include <Numtool/tensorUtils.hpp>
#include <Numtool/tensor.hpp>
#include <iostream>
#pragma once
namespace OPTIMIZATION {

    template<typename T, typename IDX>
    class SALM_Linear_LHS_Dense{
        private:
        MATH::TENSOR::Matrix<T, IDX> *jac;
        T alpha;
        T normFPower;

        public:
        IDX getM() const {return jac->getN(); }

        IDX getN() const {return jac->getN(); }

        void setJac( MATH::TENSOR::Matrix<T, IDX> &jac_){
            jac = &jac_;
        }

        void setValues(T alpha_, T normFPower_){
            alpha = alpha_;
            normFPower = normFPower_;
        }

        void mult(const T *x, T *out) const {
            jac->ATAx(x, out);
            for(IDX i = 0; i < jac->getN(); i++) out[i] += alpha * normFPower * x[i];
        }
    };


    /**
     * @brief Self-Adaptive Levenberg Marquardt
     * Currently only supports dense matrices
     * 
     * @tparam T The floating point type
     * @tparam IDX the index type
     */
    template<typename T, typename IDX>
    class SALM {
        public:
        // ---------------------
        // - SOLVER PARAMETERS -
        // ---------------------
        T tau_abs; /// Absolute Error
        T tau_rel; /// Relative Error
        IDX kmax;   /// Maximum allowed iterations
        T p0;       /// the ratio cutoff for accepting the iteration

        T min_lambda = 0; /// Minimum allowed regularization parameter
        T lambda_0 = 0.1; /// initial regularization parameter

        T krylov_epsilon = 1e-8;


        T q(T r){
            return std::min((T) 3.0 , std::max((T) 0.25, 1 - 2 * CUBED(2 * r - 1)));
        }

        /**
         * @brief Construct a new SALM Solver
         * 
         * @param tau_abs the absolute tolerance for error (residual must be less than this)
         * @param tau_rel the relative tolerance on error residual < residual0 * tau_rel + tau_abs
         * @param kmax the maximum number of iterations
         * @param p0 the ratio cutoff for accepting the iteration
         * @param min_lambda the minimum allowed regularization parameter
         * @param lambda_0 the initial regularization parameter
         * @param krylov_epsilon the error tolerance for krylov iterations
         */
        SALM(
            T tau_abs = 1e-8,
            T tau_rel = 1e-8,
            IDX kmax = 100,
            T p0 = 0.25,
            T min_lambda = 0,
            T lambda_0 = 1e-4,
            T krylov_epsilon = 1e-8
        ) : tau_abs(tau_abs), tau_rel(tau_rel), kmax(kmax), p0(p0), 
        min_lambda(min_lambda), lambda_0(lambda_0), krylov_epsilon(krylov_epsilon) {}

        void setLambda0(T lambda_0_arg){lambda_0 = lambda_0_arg;}
        /**
         * @brief Solve the nonlinear problem using the cost function
         * 
         * @param cost the cost function
         * @param x set x to the initial guess or zero fill
         * @return the final lambda
         */
        T solve(CostFunction<T, IDX> &cost, MATH::TENSOR::Vector<T, IDX> &x, std::vector<T> &residuals){
            using namespace MATH::TENSOR;
            MATH::TENSOR::Vector<T, IDX> p(cost.getN());
            MATH::TENSOR::Vector<T, IDX> g(cost.getN()); // JT * r
            MATH::TENSOR::Matrix<T, IDX> jac(cost.getM(), cost.getN());
            MATH::TENSOR::Vector<T, IDX> xstep(cost.getN());
            MATH::TENSOR::Vector<T, IDX> r(cost.getM());
            MATH::TENSOR::Vector<T, IDX> rstep(cost.getM());
            MATH::TENSOR::Vector<T, IDX> temp(cost.getM()); // size m scratch space
            SALM_Linear_LHS_Dense<T, IDX> linearSubproblem{};
            linearSubproblem.setJac(jac);
            p.zeroFill();

            T lambda = lambda_0;
            cost.eval(x.data, r.data);
            T r0 = r.norm();
            IDX k;
            for(k = 0; k < kmax; k++){ // main loop
                cost.jacobianDense(x.data, jac);
                T rk = r.norm();
                residuals.push_back(rk);
                if(rk < tau_abs + r0 * tau_rel) break; // check for convergence

                // solve the linear subproblem with conjugate gradient
                jac.TransposeMultiply(r, g);
                for(IDX i = 0; i < cost.getN(); i++) g[i] = -g[i];
                linearSubproblem.setValues(lambda, SQUARED(rk));

                KRYLOV::bicgstab<T, IDX, SALM_Linear_LHS_Dense<T, IDX>>(
                    linearSubproblem, p.data, g.data, krylov_epsilon, std::max((IDX) 10, cost.getN())
                );

                // compute actual and predicted reduction
                xstep = x;
                xstep += p;
                cost.eval(xstep.data, rstep.data);
                T rkp1 = rstep.norm();
                T ared = SQUARED(rk) - SQUARED(rkp1);
                jac.mult(p.data, temp.data);
                temp += r;
                T pred = SQUARED(rk) - SQUARED(temp.norm());

                T ratio = ared / pred;
                if(ratio > p0) { // if ratio is acceptable take the step
                    x = xstep;
                    r = rstep;
                }

                lambda = std::max(min_lambda, lambda * q(ratio));
            }
            std::cout << "ALM finished in: " << k << " iterations.\n";
            return lambda;
        }

    };

    /**
     * @brief Levenberg Marquardt
     * No adaptation
     * Currently only supports dense matrices
     * 
     * @tparam T The floating point type
     * @tparam IDX the index type
     */
    template<typename T, typename IDX>
    class LevenbergMarquardt {
        private:
        // ---------------------
        // - SOLVER PARAMETERS -
        // ---------------------
        T tau_abs; /// Absolute Error
        T tau_rel; /// Relative Error
        IDX kmax;   /// Maximum allowed iterations
        T p0;       /// the ratio cutoff for accepting the iteration

        T min_lambda = 0; /// Minimum allowed regularization parameter
        T lambda_0 = 0.1; /// initial regularization parameter

        T krylov_epsilon = 1e-8;


        T q(T r){
            return std::min((T) 3.0 , std::max((T) 0.25, 1 - 2 * CUBED(2 * r - 1)));
        }

        public:

        /**
         * @brief Construct a new SALM Solver
         * 
         * @param tau_abs the absolute tolerance for error (residual must be less than this)
         * @param tau_rel the relative tolerance on error residual < residual0 * tau_rel + tau_abs
         * @param kmax the maximum number of iterations
         * @param p0 the ratio cutoff for accepting the iteration
         * @param min_lambda the minimum allowed regularization parameter
         * @param lambda_0 the initial regularization parameter
         * @param krylov_epsilon the error tolerance for krylov iterations
         */
        LevenbergMarquardt(
            T tau_abs = 1e-8,
            T tau_rel = 1e-8,
            IDX kmax = 100,
            T p0 = 0.25,
            T min_lambda = 0,
            T lambda_0 = 1e-4,
            T krylov_epsilon = 1e-8
        ) : tau_abs(tau_abs), tau_rel(tau_rel), kmax(kmax), p0(p0), 
        min_lambda(min_lambda), lambda_0(lambda_0), krylov_epsilon(krylov_epsilon) {}

        void setLambda0(T lambda_0_arg){lambda_0 = lambda_0_arg;}
        /**
         * @brief Solve the nonlinear problem using the cost function
         * 
         * @param cost the cost function
         * @param x set x to the initial guess or zero fill
         * @return the final lambda
         */
        T solve(CostFunction<T, IDX> &cost, MATH::TENSOR::Vector<T, IDX> &x, std::vector<T> &residuals){
            using namespace MATH::TENSOR;
            MATH::TENSOR::Vector<T, IDX> p(cost.getN());
            MATH::TENSOR::Vector<T, IDX> g(cost.getN()); // JT * r
            MATH::TENSOR::Matrix<T, IDX> jac(cost.getM(), cost.getN());
            MATH::TENSOR::Vector<T, IDX> xstep(cost.getN());
            MATH::TENSOR::Vector<T, IDX> r(cost.getM());
            MATH::TENSOR::Vector<T, IDX> rstep(cost.getM());
            MATH::TENSOR::Vector<T, IDX> temp(cost.getM()); // size m scratch space
            SALM_Linear_LHS_Dense<T, IDX> linearSubproblem{};
            linearSubproblem.setJac(jac);
            p.zeroFill();

            T lambda = lambda_0;
            cost.eval(x.data, r.data);
            T r0 = r.norm();
            IDX k;
            for(k = 0; k < kmax; k++){ // main loop
                cost.jacobianDense(x.data, jac);
                T rk = r.norm();
                residuals.push_back(rk);
                if(rk < tau_abs + r0 * tau_rel) break; // check for convergence

                // solve the linear subproblem with conjugate gradient
                jac.TransposeMultiply(r, g);
                for(IDX i = 0; i < cost.getN(); i++) g[i] = -g[i];
                linearSubproblem.setValues(lambda, SQUARED(rk));

                KRYLOV::bicgstab<T, IDX, SALM_Linear_LHS_Dense<T, IDX>>(
                    linearSubproblem, p.data, g.data, krylov_epsilon, std::max((IDX) 10, cost.getN())
                );

                // compute actual and predicted reduction
                xstep = x;
                xstep += p;
                cost.eval(xstep.data, rstep.data);

                x = xstep;
                r = rstep;
            }
            std::cout << "LM finished in: " << k << " iterations.\n";
            return lambda;
        }

    };
}
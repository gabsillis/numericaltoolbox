/**
 * @file CRSStorage.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Compressed Row Storage Definition
 * @version 0.1
 * @date 2022-12-31
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
#include "umpire/TypedAllocator.hpp"
#include "umpire/Allocator.hpp"
#include "umpire/ResourceManager.hpp"

namespace MATH::TENSOR {

    /**
     * @brief Compressed Row Storage
     * Contiguous Storage for nonuniform 2d arrays
     * @tparam T the data type
     */
    template<typename T>
    class CRS {
        private:
        umpire::TypedAllocator<T> allocator;

        public:
        /// @brief  the number of rows
        std::size_t n;

        /// @brief the total number of elements 
        /// (number of nonzeros from sparse matrix terminology)
        std::size_t nnz;

        /**
         * @brief Location information for the data
         * for row i:
         * loc[i] = pointer to the start of the data
         * loc[i+1] = pointer to one past the end of the data
         */
        std::vector<T *> loc;

        /// @brief  The data stored in the CRS matrix
        T *data;


        // ================
        // = Constructors =
        // ================

        private:
        void createLoc(std::vector<std::ptrdiff_t> &sizes){
            loc[0] = data;
            for(int isz = 0; isz < sizes.size(); ++isz){
                loc[isz + 1] = loc[isz] + sizes[isz];
            }

        }

        public:

        /**
         * @brief Default constructor
         * Uses host allocator, does not allocate any memory for the data
         */
        CRS() : allocator(umpire::ResourceManager::getInstance().getAllocator("HOST")), n(0), nnz(0), loc() {
            data = allocator.allocate(0);
        }

        /**
         * @brief Construct the compressed row storage to specified size
         * @param sizes the number of elements in each row
         * @param totalData the total number of elements
         * @param allocator_arg the umpire allocator to use (defaults to HOST)
         */
        CRS(
            std::vector<std::ptrdiff_t> &sizes,
            std::size_t totalData,
            umpire::Allocator allocator_arg = umpire::ResourceManager::getInstance().getAllocator("HOST")
        ) : allocator(allocator_arg), n(sizes.size()), nnz(totalData), loc(sizes.size() + 1){
            data = allocator.allocate(totalData);
            createLoc(sizes);
        }

        /**
         * @brief Construct a new CRS from a 2d initialzier list
         * 
         * @param values the values to assign
         * @param allocator_arg the allocator to use (defaults to HOST)
         */
        CRS(
            std::initializer_list<std::initializer_list<T>> values,
            umpire::Allocator allocator_arg = umpire::ResourceManager::getInstance().getAllocator("HOST")
        ) : allocator(allocator_arg), loc(){
            nnz = 0;
            n = values.size();
            // count total data
            for(auto row : values){
                nnz += row.size();
            }

            // allocate data
            data = allocator.allocate(nnz);
            loc.resize(n + 1);

            // loop through initialzier list to assign data and loc
            T *datafollower = data;
            loc[0] = data;
            int irow = 0;
            for(auto row : values){
                for(auto val : row){
                    *datafollower = val;
                    datafollower++;
                }
                loc[irow + 1] = datafollower;
                irow++;
            }
        }

        /**
         * @brief Copy Constructor
         * 
         * @param other the CRS to copy
         */
        CRS(const CRS<T> &other)
        : allocator(other.allocator), n(other.n), nnz(other.nnz), loc(other.n + 1) {
            // allocate data
            data = allocator.allocate(nnz);

            // copy data
            auto &rm = umpire::ResourceManager::getInstance();
            rm.copy(data, other.data);

            // remake location pointer
            std::vector<std::ptrdiff_t> sizes(n);
            for(int isz = 0; isz < n; ++isz) 
                sizes[isz] = other.loc[isz + 1] - other.loc[isz];
            createLoc(sizes);
        }

        /**
         * @brief Move Constructor
         * 
         * @param other the CRS to move 
         */
        CRS(CRS<T> &&other)
        : allocator(other.allocator), n(other.n), nnz(other.nnz),
          loc(std::move(other.loc)), data(other.data)
        {
            // clean up other
            other.nnz = 0; other.n = 0;
            other.data = nullptr;
        }

        ~CRS(){
            allocator.deallocate(data, nnz);
        }


        // ========================
        // = Assignment Operators =
        // ========================
        CRS<T> &operator=(const CRS<T> &other){
            allocator.deallocate(data, nnz);
            allocator = other.allocator;
            n = other.n;
            nnz = other.nnz;
            data = allocator.allocate(nnz);

            // copy data
            auto &rm = umpire::ResourceManager::getInstance();
            rm.copy(data, other.data);

            loc.resize(n);
            std::vector<std::ptrdiff_t> sizes(n);
            for(int isz = 0; isz < n; ++isz) 
                sizes[isz] = other.loc[isz + 1] - other.loc[isz];
            createLoc(sizes);
            return *this;
        }

        CRS<T> &operator=(CRS<T> &&other){
            allocator.deallocate(data, nnz);
            allocator = other.allocator;
            n = other.n; other.n = 0;
            nnz = other.nnz; other.nnz = 0;
            data = other.data;
            other.data = nullptr;
            loc = std::move(other.loc);
            return *this;
        }


        // ============
        // = Indexing =
        // ============
        /**
          * @brief Gets the number of entries in the row
          * 
          * @param j the row
          * @return int the number of entries in row j
          */
        inline std::ptrdiff_t rowSize(std::size_t j){
            return (loc[j+1] - loc[j]);
        }

         /**
          * @brief Get the start pointer for a row
          * 
          * @param j the row
          * @return T* the start pointer
          */
        inline T* start(std::size_t j){
            return loc[j];
        }

         /**
          * @brief Get the end pointer for a row
          * 
          * @param j the row
          * @return T* 1 past the last entry in that row
          */
        inline T* end(std::size_t j){
            return loc[j+1];
        }

         /**
          * @brief Get the array for a row
          * 
          * @param j the row
          * @return T* the array for this row, use rowsize() for bounds
          */
        inline T* operator[](std::size_t j){
            return start(j);
        }

    };
}



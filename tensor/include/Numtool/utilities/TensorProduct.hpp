/**
 * @file TensorProduct.hpp
 * @author your name (you@domain.com)
 * @brief Tensor products
 * @version 0.1
 * @date 2022-04-18
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once
#include <concepts>
#include <ostream>
#include <tuple>
#include <vector>
#include <Numtool/Errors.h>
#include <Numtool/CompGeo.hpp>
#include <Numtool/TMPDefs.hpp>
#include <Numtool/MathUtils.hpp>
namespace MATH::TENSOR_PROD {

    #if __cplusplus > 201703L

    template<typename Polyfunc, typename T, int k>
    concept Function1Variate = requires(
        Polyfunc &func,
        T xi
    ){
        { func.template eval<0>(xi) } -> std::convertible_to<T>;
        // TODO:: use std::integer_sequence and pack expansion to ensure all in between functions are available
        { func.template eval<k>(xi) } -> std::convertible_to<T>;

        { func.template derivative<0>(xi) } -> std::convertible_to<T>;
        // TODO:: use std::integer_sequence and pack expansion to ensure all in between functions are available
        { func.template derivative<k>(xi) } -> std::convertible_to<T>;

        { func.template derivative2nd<0>(xi) } -> std::convertible_to<T>;
        // TODO:: use std::integer_sequence and pack expansion to ensure all in between functions are available
        { func.template derivative2nd<k>(xi) } -> std::convertible_to<T>;
    };

    template<typename Polyfunc, typename T>
    concept Function1VariateRuntime = requires(
        Polyfunc &func,
        T xi,
        int k
    ){
        { func.eval(k, xi) } -> std::convertible_to<T>;

        { func.derivative(k, xi) } -> std::convertible_to<T>;

        { func.derivative2nd(k, xi) } -> std::convertible_to<T>;

        // Ensure up to kth derivatives are available for each func
        {func.jthDerivative(k, k, xi)} ->std::convertible_to<T>;

        // { func.template derivative2nd<0>(xi) } -> std::convertible_to<T>;
        // // TODO:: use std::integer_sequence and pack expansion to ensure all in between functions are available
        // { func.template derivative2nd<k>(xi) } -> std::convertible_to<T>;
    };

    #endif

    /**
     * The set of all Multi Indices alpha
     * where Linf norm of alpha <= order
     *
     * @tparam ndim the number of dimensions
     */
    template<int ndim>
    struct MultiIndexSet{
        using MultiIndex = std::array<int, ndim>;

        // The set of all multi indices in a std::vector
        std::vector<MultiIndex> indices;
        int size;

        private:
        template<int idim>
        MultiIndex incrementLinf(MultiIndex &prev, int order){
            MultiIndex out = prev;
            
            // note: could be more efficient, but this is a precalculation
            out[idim]++;
            if(out[idim] > order){
                out[idim] = 0;
                
                if constexpr(idim < ndim){
                    return incrementLinf<idim + 1>(out, order);
                } else {
                    for(int jdim = 0; jdim < ndim; ++jdim) out[jdim] = 0;
                    return out;
                }
            } else {
                return out;
            }
        }

        public:
        /**
         * @brief constructor
         * @param order the order of the multi index
         */
        MultiIndexSet(int order){
            MultiIndex first{};
            // first multi index = 0
            for(int idim = 0; idim < ndim; ++idim) first[idim] = 0;
            indices.push_back(first);

            size = MATH::integer_pow(order + 1, ndim); // [0, k] inclusive in each dimension
            for(int i = 1; i < size; ++i) {
                MultiIndex &prev = indices[i - 1];
                indices.push_back(incrementLinf<0>(prev, order));
            }
        }

        MultiIndex &operator[](int i){return indices[i];}
    };

    template<int ndim>
    struct MultiIndexAlt{
        int indices[ndim] = {0};

        inline int get(int idim){ return indices[idim]; }

        bool increment(int imax){
            int idim = ndim - 1;
            while(idim >= 0){
                indices[idim]++;
                if(indices[idim] > imax){
                    indices[idim] = 0;
                    idim--;
                } else {
                    return true;
                }
            }
            return false; // increment unsuccessful
        }

        inline int &operator[] (int idim){ return indices[idim]; }
        

        void print(std::ostream &out){
            out << "MultiIndex: {" << indices[0];
            for(int i = 1; i < ndim; i++) out << ", " << indices[i];
            out << "}\n";
        }
    };

    /**
     * @brief Multi Index where a position can be locked to generate
     * all the multi indices with the given position unchanging
     * 
     * @tparam ndim number of dimensions
     * @tparam idxmax maximum index value + 1
     *          e.g for P1 idxmax = 2 b/c fcn can be p0 or p1
     */
    template<int ndim, int idxlim>
    struct LockedMultiIndex {
        int idxs[ndim] = {0};
        int lockedDim = 0;

        LockedMultiIndex() = default;

        LockedMultiIndex(int lockedDim) : lockedDim(lockedDim) {}

        /**
         * @brief get the equivalent 1d array for the multi index
         * 
         * @return int the index in a 1d array
         */
        int globalIdx(){
            int mult = 1;
            int ret = 0;
            for(int idim = ndim - 1; idim >= 0; idim--){
                ret += idxs[idim] * mult;
                mult *= idxlim;
            }
            return ret;
        }

        /**
         * @brief increment the multi-index
         * 
         * @return true if the increment results in a valid multi index
         * @return false otherwise, this means to stop incrementing
         */
        bool increment(){
            int idim = ndim - 1;
            if(lockedDim == idim) idim--;
            while(idim >= 0){
                idxs[idim]++;
                if(idxs[idim] >= idxlim){
                    idxs[idim] = 0;
                    if(lockedDim == idim - 1) idim -= 2;
                    else idim--;
                } else {
                    return true;
                }
            }
            return false; // increment unsuccessful
        }
    };

    template<int ndim>
    struct DimensionalIndices{
        typedef typename MATH::TMP::sized_tuple<ndim, int>::tuple_type type;
    };

    template<> struct DimensionalIndices<1> { typedef std::tuple<int> type; };
    template<> struct DimensionalIndices<2> { typedef std::tuple<int, int> type; };
    template<> struct DimensionalIndices<3> { typedef std::tuple<int, int, int> type; };
    template<> struct DimensionalIndices<4> { typedef std::tuple<int, int, int, int> type; };
    template<typename T, int ndim>
    class MultidimDerivativeAccessor {
        /**
         * @brief Get the Derivative for given indices
         * 
         * @param iderivatives the order of the derivative in each dimension
         *      Derivatives are accesed in order x, y, z, ...
         *      i.e for 3D a tuple with values 3, 4, and 2 would
         *      return f_xxxyyyyzz (3 x derivatives, 4 y derivatives, and 2 z derivatives)
         *      NOTE: this means that access will be z-fastest
         * @return T the value of the given derivative
         */
        virtual T getDerivative(typename DimensionalIndices<ndim>::type iderivatives) = 0;
    };

    /**
     * @brief Multidimensional derivative storage
     * 
     * @tparam T the floating point type
     * @tparam ndim the number of dimensions
     * @tparam nfunc the number of basis functions
     */
    template<typename T, int ndim, int nfunc>
    class MultidimDerivatives final : public MultidimDerivativeAccessor<T, ndim>{

        private:
        int stride;
        int calculateStorage(int max_derivative_order){
            return integer_pow(max_derivative_order + 1, ndim);
        }

        template<int idim>
        inline int strideIndex(typename DimensionalIndices<ndim>::type iderivatives, int stridemult){
            if constexpr (idim < ndim){
                return std::get<idim>(iderivatives) * stridemult 
                    + strideIndex<idim + 1>(iderivatives, stridemult * stride);  
            } else {
                return stridemult * std::get<ndim>(iderivatives);
            }
        }

        public:
        T *data;
        MultidimDerivatives(int max_derivative_order)
        : stride(max_derivative_order + 1),
          data{new T[calculateStorage(max_derivative_order)]}
        {}

        /**
         * @brief Get the Derivative for given indices
         * 
         * @param iderivatives the order of the derivative in each dimension
         *      Derivatives are accesed in order x, y, z, ...
         *      i.e for 3D a tuple with values 3, 4, and 2 would
         *      return f_xxxyyyyzz (3 x derivatives, 4 y derivatives, and 2 z derivatives)
         *      NOTE: this means that access will be z-fastest
         * @return T the value of the given derivative
         */
        T getDerivative(decltype(DimensionalIndices<ndim>::type) iderivatives) override {
            return data[strideIndex<0>(iderivatives, 1)];
        }

        T &derivRef(decltype(DimensionalIndices<ndim>::type) iderivatives) {
            return data[strideIndex<0>(iderivatives, 1)];
        }

        ~MultidimDerivatives(){delete[] data;}
    };

    /**
     * @brief Full Tensor product
     * the multi-index set consists of all multi-indices with the Linf norm <= k
     * 
     * @tparam T the numeric type
     * @tparam Polyfunc the polynomial function
     * @tparam ndim the dimensionality
     * @tparam k the polynomial order
     */
    template<typename T, typename Polyfunc, int ndim, int k>
    class LinfTensorProduct{
        private:
        Polyfunc func;
        MultiIndexSet<ndim> index_set;

        #if __cplusplus > 201703L
        static_assert(Function1VariateRuntime<Polyfunc, T>, "This function doesn't satisfy the Function1Variate Concept");
        #endif


        template<int a, int b>
        static constexpr int pow_const(){
            if constexpr (b == 0) return 1;
            else return pow_const<a, b-1>() * a;
        }


        public:

        LinfTensorProduct() : index_set(k) {}

        static constexpr int cardinality(){
            return pow_const<k + 1, ndim>();
        }

        inline T getProduct(const T *xi, int i){
            const std::array<int, ndim> &mindex = index_set[i];
            T prod = func.eval(mindex[0], xi[0]);
            for(int idim = 1; idim < ndim; ++idim){
                prod *= func.eval(mindex[idim], xi[idim]);
            }
            return prod;
        }

        inline void getGradient(const T *xi, int ibasis, T grad[ndim]){
            const std::array<int, ndim> &mindex = index_set[ibasis];

            // first dimension (all derivative directions)
            grad[0] = func.derivative(mindex[0], xi[0]);
            for(int ideriv = 1; ideriv < ndim; ++ideriv){
                grad[ideriv] = func.eval(mindex[0], xi[0]);
            }

            // all other dimensions
            for(int idim = 1; idim < ndim; ++idim){
                // evals
                for(int ideriv = 0; ideriv < idim; ++ideriv) grad[ideriv] *= func.eval(mindex[idim], xi[idim]);

                // derivative
                grad[idim] *= func.derivative(mindex[idim], xi[ndim]);
                
                // continue evals
                for(int ideriv = idim + 1; ideriv < ndim; ++ideriv) grad[ideriv] *= func.eval(mindex[idim], xi[idim]);
            }
        }

        inline void getHessian(const T *xi, int ibasis, T hess[ndim][ndim]){
            const std::array<int, ndim> &mindex = index_set[ibasis];

            int idim = 0; // first do idim = 0 for assignment, then multiply
            for(int ideriv = 0; ideriv < ndim; ++ideriv){
                for(int jderiv = 0; jderiv < ndim; ++jderiv){
                    if(idim == ideriv && idim == jderiv){ // hope optimizing compiler can deal with the if statements b/c much more readable
                        hess[ideriv][jderiv] = func.derivative2nd(mindex[idim], xi[idim]);
                    } else if (idim == ideriv || idim == jderiv){
                        hess[ideriv][jderiv] = func.derivative(mindex[idim], xi[idim]);
                    } else {
                        hess[ideriv][jderiv] = func.derivative(mindex[idim], xi[idim]);
                    }
                }
            }

            // multiplication loop
            for(int idim = 1; idim < ndim; ++idim){
                for(int ideriv = 0; ideriv < ndim; ++ideriv){
                    for(int jderiv = 0; jderiv < ndim; ++jderiv){
                        if(idim == ideriv && idim == jderiv){ // hope optimizing compiler can deal with the if statements b/c much more readable
                            hess[ideriv][jderiv] *= func.derivative2nd(mindex[idim], xi[idim]);
                        } else if (idim == ideriv || idim == jderiv){
                            hess[ideriv][jderiv] *= func.derivative(mindex[idim], xi[idim]);
                        } else {
                            hess[ideriv][jderiv] *= func.derivative(mindex[idim], xi[idim]);
                        }
                    }
                }
            }
        }
    };

    /**
     * @brief Tensor product where the product is at most k-degree
     * the multi-index set consists of all multi-indices with the l1 norm <= k
     * 
     * @tparam T the numeric type
     * @tparam Polyfunc the polynomial function
     * @tparam ndim the dimensionality 
     * @tparam k the degree of the polynomial
     */
    template<typename T, typename Polyfunc, int ndim, int k>
    class OrderLimitedTensorProduct{
        private:
        Polyfunc func;

        #if __cplusplus > 201703L
        static_assert(Function1Variate<Polyfunc, T, k>);
        #endif

        template<int d, int p>
        inline constexpr int sizeQ(){
            return MATH::binomial<p + d - 1, d - 1>();
        }
        
        template<int d, int j, int p>
        inline void Qpd(const T *xi, T *result){
            constexpr int idim = d - 1; // array index for xi
            // base case: d = 1
            // multiply by polynomial order p
            if constexpr (d == 1){
                result[0] = func.template eval<p>(xi[idim]);
            } else {
                // drill down 
                // start the new j from 0
                // p - j because we want overall order to be p
                Qpd<d - 1, 0, p - j>(xi, result);
                // block multiply
                T qj = func.template eval<j>(xi[idim]); // keep to single function evaluation
                for(int l = 0; l < sizeQ<d - 1, p - j>(); l++) result[l] *= qj;

                // move to next "block" of polynomial order j + 1
                if constexpr (j < p){
                    Qpd<d, j + 1, p>(xi, result + sizeQ<d - 1, p - j>());
                }
            }
        }

        template<int d, int i, int p>
        inline void gradQpdOld(const T *xi, T **grad){ // BASED ON PRODUCT RULE, LESS EFFICIENT
            constexpr int idim = d - 1; // array index for xi
            // base case : d = 1
            // value is delta^j_1 dQ^i/d\xi (x_1)
            if constexpr(d == 1){
                // only the gradient wrt 1st variable is nonzero for this
                grad[0][idim] = func.template derivative<p>(xi[idim]); 
                for(int j = 1; j < ndim; j++) grad[0][j] = 0.0;
            } else {
                // Product Rule
                // second term: drill down
                gradQpd<d - 1, 0, p - i>(xi, &grad[0]);
                // block multiply through by q^i(x_d)
                T qid = func.template eval<i>(xi[idim]);
                for(int l = 0; l < sizeQ<d-1, p-i>(); l++){
                    for(int j = 0; j < ndim; j++) grad[l][j] *= qid;
                }

                // kronkeker delta over loop
                constexpr int j = idim;
                T basisEval[sizeQ<d - 1, p - i>()];
                Qpd<d-1, 0, p-i>(xi, basisEval);
                // add the first term of the product rule
                T dbidxj = func.template derivative<i>(xi[j]);
                for(int l = 0; l < sizeQ<d-1, p-i>(); l++)
                    grad[l][j] = grad[l][j] + dbidxj * basisEval[l];

                // move to the next "block"
                if constexpr (i < p){
                    gradQpd<d, i + 1, p>(xi, &grad[sizeQ<d - 1, p - i>()]);
                }
            }
        }

        template<int d, int i, int p>
        inline void gradQpd(const T *xi, T **grad){
            constexpr int idim = d - 1; // array index for xi
            // base case : d = 1
            // value is delta^j_1 dQ^i/d\xi (x_1)
            if constexpr(d == 1){
                // only the gradient wrt 1st variable is nonzero for this
                grad[0][idim] = func.template derivative<p>(xi[idim]); 
                for(int j = 1; j < ndim; j++) grad[0][j] = func.template eval<p>(xi[idim]);
            } else {
                // drill down first
                gradQpd<d - 1, 0, p - i>(xi, &grad[0]);

                // case 1: j = d
                constexpr int j = idim;
                T basisEval[sizeQ<d - 1, p - i>()];
                Qpd<d-1, 0, p-i>(xi, basisEval);
                // add the first term of the product rule
                T dbidxj = func.template derivative<i>(xi[j]);
                for(int l = 0; l < sizeQ<d-1, p-i>(); l++)
                    grad[l][j] = grad[l][j] * dbidxj;

                // case 2: j != d
                // block multiply through by q^i(x_d)
                T qid = func.template eval<i>(xi[idim]);
                for(int l = 0; l < sizeQ<d-1, p-i>(); l++){
                    for(int j2 = 0; j2 < idim; j2++) grad[l][j2] *= qid;
                    for(int j2 = idim + 1; j2 < ndim; j2++) grad[l][j2] *= qid;
                }

                // move to the next "block"
                if constexpr (i < p){
                    gradQpd<d, i + 1, p>(xi, &grad[sizeQ<d - 1, p - i>()]);
                }
            }
        }

        template<int d, int i, int p>
        inline void hessDiagQpd(const T *xi, T **grad){
            constexpr int idim = d - 1; // array index for xi
            // base case : d = 1
            // value is delta^j_1 dQ^i/d\xi (x_1)
            if constexpr(d == 1){
                // only the gradient wrt 1st variable is nonzero for this
                grad[0][idim] = func.template derivative2nd<p>(xi[idim]); 
                for(int j = 1; j < ndim; j++) grad[0][j] = func.template eval<p>(xi[idim]);
            } else {
                // drill down first
                hessDiagQpd<d - 1, 0, p - i>(xi, &grad[0]);

                // case 1: j = d
                constexpr int j = idim;
                T basisEval[sizeQ<d - 1, p - i>()];
                Qpd<d-1, 0, p-i>(xi, basisEval);
                // add the first term of the product rule
                T dbidxj = func.template derivative2nd<i>(xi[j]);
                for(int l = 0; l < sizeQ<d-1, p-i>(); l++)
                    grad[l][j] = grad[l][j] * dbidxj;

                // case 2: j != d
                // block multiply through by q^i(x_d)
                T qid = func.template eval<i>(xi[idim]);
                for(int l = 0; l < sizeQ<d-1, p-i>(); l++){
                    for(int j2 = 0; j2 < idim; j2++) grad[l][j2] *= qid;
                    for(int j2 = idim + 1; j2 < ndim; j2++) grad[l][j2] *= qid;
                }

                // move to the next "block"
                if constexpr (i < p){
                    hessDiagQpd<d, i + 1, p>(xi, &grad[sizeQ<d - 1, p - i>()]);
                }
            }
        }

        template<int i>
        inline void product(const T *xi, T *result){
            Qpd<ndim, 0, i>(xi, result);
            if constexpr (i < k){
                product<i + 1>( xi, &result[sizeQ<ndim, i>()] );
            }
        }

        template<int i>
        inline void gradproduct(const T *xi, T **result){
            gradQpd<ndim, 0, i>(xi, result);
            if constexpr (i < k){
                gradproduct<i + 1>( xi, &result[sizeQ<ndim, i>()] );
            }
        }

        template<int i>
        inline void hessDiagproduct(const T *xi, T **result){
            hessDiagQpd<ndim, 0, i>(xi, result);
            if constexpr (i < k){
                hessDiagproduct<i + 1>( xi, &result[sizeQ<ndim, i>()] );
            }
        }

        public:
        inline constexpr int cardinality(){ return MATH::binomial<k + ndim, ndim>(); }

        static inline constexpr int cardinalityStatic(){
            return MATH::binomial<k + ndim, ndim>();
        }

        /**
         * @brief Get the tensor product evaluated into an array
         * 
         * @param [in] xi the abscisse
         * @param [out] result the evaluated tensor product, size = cardinality()
         */
        void getProduct(const T *xi, T *result){
            product<0>(xi, result);
        }

        void getGradient(const T *xi, T **grad){
            gradproduct<0>(xi, grad);
        }

        void hessianDiagonal(const T *xi, T **HessDiag){
            hessDiagproduct<0>(xi, HessDiag);
        }

    };
}

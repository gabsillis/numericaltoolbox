/**
 * @file tensorclass.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Tensor Classes
 * @version 0.1
 * @date 2021-05-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include <initializer_list>
namespace MATH {
    /**
     * @brief Namespace for Tensor classes and methods
     * 
     * A Tensor is a multidimensional representation of values accessed through indical notation
     * Tensors mathematically have variance properties attributed to them, though many of the classes here can be repurposed for multi-indexing purposes
     */
    namespace TENSOR {

        template<typename T, std::size_t ndim>
        class Tensor {
            private:
            /**
             * @brief The sizes of each dimension
             */
            std::ptrdiff_t dims[ndim];

            /**
             * @brief The product of all dimensions after that index 
             * i.e dimsProd0 = dims[1] * dims[2] * ... 
             * the last dimension is omitted because it is 1
             * Currently this disallows size 1 tensors
             */
            std::ptrdiff_t dimsProd[ndim - 1];

            /**
             * @brief The data stored in the tensor
             */
            T* data;

            /**
             * @brief tell if the data is owned or not
             * 
             */
            bool dataowned;

            public:

            /**
             * @brief Construct a new Tensor object from a variadic list of size arguments
             * 
             * @tparam I the size argument type
             * @param args the size arguments
             */
            template<typename ...I>
            Tensor(I ... args) : dataowned(true) {
                static_assert(sizeof...(I) == ndim);
                dimsInit<0>(args...);
                data = new T[dimsProd[0] * dims[0]];
            }

            template<int d, typename idx, typename ...I>
            void dimsInit(idx arg, I ... args){
                dims[d] = arg;
                dimsProd[d] = 1;
                for(int j = 0; j < d; j++){
                    dimsProd[j] *= arg;
                }
                dimsInit<d + 1>(args...);
            }

            template<int d>
            void dimsInit(){}

            /**
             * @brief Construct a new Tensor object
             * 
             * @param dim_args must be the same amount of arguments as number of dimensions
             */
            Tensor(std::initializer_list<std::size_t> dim_args) : dataowned(true) {
                
                int d = 0;
                for(auto dim_arg = dim_args.begin(); dim_arg != dim_args.end(); d++, dim_arg++){
                    dims[d] = *dim_arg;
                    dimsProd[d] = 1;
                    for(int j = 0; j < d; j++){
                        dimsProd[j] *= *dim_arg;
                    }
                }
                data = new T[dimsProd[0] * dims[0]];
            }

            /**
             * @brief Construct a new Tensor object on an existing array
             * DOES NOT TAKE OWNERSHIP OF THE ARRAY
             * 
             * @param dim_args must be the same amount of arguments as number of dimensions
             */
            Tensor(double *data, std::initializer_list<std::size_t> dim_args) : data(data), dataowned(false) {
                
                int d = 0;
                for(auto dim_arg = dim_args.begin(); dim_arg != dim_args.end(); d++, dim_arg++){
                    dims[d] = *dim_arg;
                    dimsProd[d] = 1;
                    for(int j = 0; j < d; j++){
                        dimsProd[j] *= *dim_arg;
                    }
                }
            }

            /**
             * @brief Construct a new Tensor object on an existing array
             * DOES NOT TAKE OWNERSHIP OF THE ARRAY
             * 
             * @param dim_args must be the same amount of arguments as number of dimensions
             */
            Tensor(double *dataarg, std::ptrdiff_t *dim_args) : dataowned(false) {
                for(int d = 0; d < ndim; d++){
                    dims[d] = dim_args[d];
                    dimsProd[d] = 1;
                    for(int j = 0; j < d; j++){
                        dimsProd[j] *= dims[d];
                    }
                }
                data = dataarg;
            }

            /**
             * @brief Copy constructor
             * 
             * @param other the other tensor to copy
             */
            Tensor(const Tensor<T, ndim> &other) : dataowned(true) {
                for(int d = 0; d < ndim; d++) dims[d] = other.dims[d];
                for(int d = 0; d < ndim - 1; d++) dimsProd[d] = other.dimsProd[d];
                data = new T[dimsProd[0] * dims[0]];
                for(int i = 0; i < dimsProd[0] * dims[0]; i++) data[i] = other.data[i];
            }

            /**
             * @brief Copy Assignment Operator
             * 
             * @param other the other Tensor to copy
             * @return Tensor<T, ndim>& a reference to this
             */
            Tensor<T, ndim> &operator=(const Tensor<T, ndim> &other){
                if(&other != this){
                    for(int d = 0; d < ndim; d++) dims[d] = other.dims[d];
                    for(int d = 0; d < ndim - 1; d++){
                        dimsProd[d] = other.dimsProd[d];
                    }
                    delete[] data;
                    data = nullptr;
                    data = new T[dimsProd[0] * dims[0]];
                    for(int i = 0; i < dimsProd[0] * dims[0]; i++){
                        data[i] = other.data[i];
                    }
                }
            }

            /**
             * @brief Destroy the Tensor object
             * 
             */
            ~Tensor(){
                if(dataowned) delete[] data;
            }

            private:
            template<typename ...indices>
            T &indexHelper(T *loc, std::size_t dim, std::ptrdiff_t dim_idx, indices... indexpack){
                if constexpr(sizeof ...(indexpack) == 0){
                    return *(loc + dim_idx);
                } else {
                    return indexHelper(loc + dimsProd[dim] * dim_idx, dim+1, indexpack...);
                }
            }

            template<typename ...indices>
            const T &indexHelper(T *loc, std::size_t dim, std::ptrdiff_t dim_idx, indices... indexpack) const{
                if constexpr(sizeof ...(indexpack) == 0){
                    return *(loc + dim_idx);
                } else {
                    return indexHelper(loc + dimsProd[dim] * dim_idx, dim+1, indexpack...);
                }
            }

            public:
            // ------------
            // - Indexing -
            // ------------

            /**
             * @brief Index into the matrix
             * 
             * @tparam indices the index paraeter pack
             * @param first the first index
             * @param indexpack the following indices
             * @return T& A reference to the value at that index
             */
            template<typename ...indices>
            T &operator() (std::ptrdiff_t first, indices... indexpack){
               if constexpr(sizeof...(indexpack) == 0){
                   return data[first];
               } else {
                   return indexHelper(data + dimsProd[0] * first, 1, indexpack...);
               }
            }

            /**
             * @brief Index into the matrix (const version)
             * 
             * @tparam indices the index paraeter pack
             * @param first the first index
             * @param indexpack the following indices
             * @return T& A reference to the value at that index
             */
            template<typename ...indices>
            const T &operator() (std::ptrdiff_t first, indices... indexpack) const{
               if constexpr(sizeof...(indexpack) == 0){
                   return data[first];
               } else {
                   return indexHelper(data + dimsProd[0] * first, 1, indexpack...);
               }

            }

            /**
             * @brief Index into the tensor by making a proxy object Tensor
             * 
             * @param idx the index
             * @return Tensor<T, ndim - 1> the proxy object
             */
            Tensor<T, ndim - 1> operator[](std::ptrdiff_t idx){
                if constexpr(ndim == 1){
                    return Tensor<T, 0>(data + idx, dims + 1);
                }
                return Tensor<T, ndim - 1>(data + dimsProd[0] * idx, dims + 1);
            }

            // -----------
            // - Utility -
            // -----------

            /**
             * @brief Get the size of the Tensor
             * 
             * @return std::size_t the Product of all dimension sizes
             */
            std::size_t totalSize(){
                return dimsProd[0] * dims[0];
            }

            /**
             * @brief Get the size for a dimension
             * 
             * @param dimension the dimension
             * @return std::size_t the size for that dimension
             */
            std::size_t dimSize(int dimension){
                return dims[dimension];
            }

            /**
             * @brief Fill with zeros
             * 
             */
            void zeroFill(){
                for(int i = 0; i < dims[0] * dimsProd[0]; i++){
                    data[i] = 0;
                }
            }

            /**
             * @brief Overload conversion to the raw pointer type by giving the data
             * 
             * @return T* the data for this tensor
             */
            operator T*() {return data; }

        };

        template<typename T>
        class Tensor<T, 0> {
            private:

            /**
             * @brief The data stored in the tensor
             */
            T* data;

            /**
             * @brief tell if the data is owned or not
             * 
             */
            bool dataowned;

            public:
            /**
             * @brief Construct a new Tensor object
             * 
             * @param dim_args must be the same amount of arguments as number of dimensions
             */
            Tensor(std::initializer_list<std::size_t> dim_args) : dataowned(true) {
                data = new T[1];
            }

            /**
             * @brief Construct a new Tensor object on an existing array
             * DOES NOT TAKE OWNERSHIP OF THE ARRAY
             * 
             * @param dim_args must be the same amount of arguments as number of dimensions
             */
            Tensor(double *dataarg, std::initializer_list<std::size_t> dim_args) : dataowned(false) {
                data = dataarg;
            }

            /**
             * @brief Construct a new Tensor object on an existing array
             * DOES NOT TAKE OWNERSHIP OF THE ARRAY
             * 
             * @param dim_args must be the same amount of arguments as number of dimensions
             */
            Tensor(double *dataarg, std::ptrdiff_t *dim_args) : dataowned(false) {
                data = dataarg;
            }

            /**
             * @brief Copy constructor
             * 
             * @param other the other tensor to copy
             */
            Tensor(const Tensor<T, 0> &other) : dataowned(true) {
                data = new T[1];
                data[0] = other.data[0];
            }

            /**
             * @brief Copy Assignment Operator
             * 
             * @param other the other Tensor to copy
             * @return Tensor<T, ndim>& a reference to this
             */
            Tensor<T, 0> &operator=(const Tensor<T, 0> &other){
                if(&other != this){
                    delete[] data;
                    data = nullptr;
                    data = new T[1];
                    data[0] = other.data[0];
                }
                return *this;
            }

            /**
             * @brief Destroy the Tensor object
             * 
             */
            ~Tensor(){
                if(dataowned) delete[] data;
            }

            public:
            // ------------
            // - Indexing -
            // ------------

            /**
             * @brief Index into the matrix
             * 
             * @param first the first index
             */
            T &operator() (){
               return *data;
            }

            /**
             * @brief Index into the matrix (const version)
             * 
             * @param first the first index
             */
            const T &operator() () const{
               return *data;
            }

            // -----------
            // - Utility -
            // -----------

            /**
             * @brief Cast a rank 0 tensor to double to allow for full array style indexing
             * 
             * @return double the data
             */
            operator double() const {
                return *data;
            }

            /**
             * @brief Allow assignment to rank zero tensor
             * 
             * @param arg the argument to assign
             * @return T& the result of the assignment
             */
            T &operator =(T arg){
                *data = arg;
                return *data;
            }

            /**
             * @brief Get the size of the Tensor
             * 
             * @return std::size_t the Product of all dimension sizes
             */
            std::size_t totalSize(){
                return 1;
            }

            /**
             * @brief Get the size for a dimension
             * 
             * @param dimension the dimension
             * @return std::size_t the size for that dimension
             */
            std::size_t dimSize(int dimension){
                return 0;
            }

            /**
             * @brief Fill with zeros
             * 
             */
            void zeroFill(){
                *data = 0;
            }

        };
    }
}
/**
 * @file matrix.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief 
 * @version 0.1
 * @date 2021-05-26
 * 
 * Dense Matrices
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#pragma once
#include <type_traits>
#include <algorithm>
#include <string.h>
#include <cstring>
#include <cmath>
#include <memory>
//#include <concepts>
#include <Numtool/MathTypes.hpp>
#include <Numtool/TensorEnums.h>
#include <Numtool/TensorConcepts.hpp>
#include <ostream>
#include <istream>
#include <iomanip>
#include <Numtool/Errors.h>
#include <Numtool/memory.hpp>

#include "umpire/Allocator.hpp"
#include "umpire/TypedAllocator.hpp"
#include "umpire/ResourceManager.hpp"

#include "RAJA/RAJA.hpp"

//#include <cblas.h>

namespace MATH {
    namespace TENSOR {

        /**
         * @brief Proxy object for indexing into column major order matrices
         * 
         * @tparam T The data type
         * @tparam IDX the index type
         */
        template<typename T, typename IDX>
        class MatrixIndexProxy{
            private:
            T **ptr;
            IDX i;

            public:
            MatrixIndexProxy(T **ptr, IDX i) : ptr(ptr), i(i) {}

            T &operator[] (IDX j){
                return ptr[j][i];
            }
        };

        template<typename T, typename IDX, ORDERING ordering>
        struct MatrixReturnType{ typedef T* type;};

        template<typename T, typename IDX>
        struct MatrixReturnType<T, IDX, COLUMN_MAJOR_ORDER>{
            typedef MatrixIndexProxy<T, IDX> type;
        };


        // forward decleration
        template<typename T, typename IDX_TYPE>
        class Vector;

        // forward declerations so operator function templates are defined for compile
        template<typename T, typename IDX, ORDERING ordering> class Matrix;

        template<typename T, typename IDX, ORDERING ordering>
        std::ostream& operator<<(std::ostream &os, const Matrix<T, IDX, ordering> &A){
            os << std::setprecision(12);
            for(IDX i = 0; i < A.m; ++i){
                os << "| ";
                for(IDX j = 0; j < A.n; ++j){
                    os << std::setw(16) << A[i][j] << " ";
                }
                os << "|\n";
            }
            return os;
        }

        template<typename T, typename IDX, ORDERING ordering>
        std::istream& operator>>(std::istream &is, Matrix<T, IDX, ordering> &A){
            int i; 
            int j;
            T val;

            while(is >> i){
                if(i < 0) break;
                is >> j >> val;
                // adjust for 1 indexing
                i -= 1;
                j -= 1;
                A[i][j] = val;
            }

            return is;
        }


        /**
         * @brief Matrix class
         * A rank 2 tensor
         * Dense storage representation
         * 
         * @tparam T The data type
         * @tparam IDX_TYPE the index type
         * @tparam ORDERING the ordering of the matrix 
         *         Note: both styles use c-style indexing Aij = A[i][j] but this changes the storage order
         *         This specifies the major dimension: the dimension which is slowest to index in
         */
        template<typename T, typename IDX_TYPE=size_t, ORDERING ordering=ROW_MAJOR_ORDER>
        class Matrix{
            private:
            // Friend the input and output stream operators
            friend std::ostream& operator<< <> (std::ostream &, const Matrix<T, IDX_TYPE, ordering> &);
            friend std::istream& operator>> <> (std::istream &, Matrix<T, IDX_TYPE, ordering> &);

            umpire::TypedAllocator<T> allocator;
            public:
            T *data; /// the data
            IDX_TYPE m, n; /// the row and column sizes
            IDX_TYPE dimsProd; /// the product of the row and column sizes
            T **ptr; /// Pointers to the start of the major dimension vectors and 1 past the end

            private:
            void createPtr(){
                if constexpr (ordering == ROW_MAJOR_ORDER){
                    ptr = new T*[m + 1];
                    for(IDX_TYPE i = 0; i <= m; i++){
                        ptr[i] = data + i * n;
                    }
                } else { // column major order
                    ptr = new T*[n + 1];
                    for(IDX_TYPE i = 0; i <= n; i++){
                        ptr[i] = data + i * m;
                    }
                }
            }
            public:

            /**
             * @brief Construct a new Matrix object
             * 
             * @param n the number of rows
             * @param m the number of columns
             */
            Matrix(const IDX_TYPE m, const IDX_TYPE n, umpire::Allocator allocator_arg = umpire::ResourceManager::getInstance().getAllocator("HOST")) 
            : allocator{allocator_arg}, m(m), n(n), dimsProd(m * n)
            {
                data = allocator.allocate(dimsProd); 
                createPtr();
            }

            /** @brief create an empty matrix */
            Matrix() : Matrix(0, 0){}

            /**
             * @brief Construct a new Matrix object from an initializer list
             * WARNING: initializer lists must all have same row lengths
             * @param values the values to add
             */
            Matrix(std::initializer_list<std::initializer_list<T>> values)
            : allocator{umpire::ResourceManager::getInstance().getAllocator("HOST")}, 
              m(values.size()), n(values.begin()->size()), dimsProd{m * n}
            {
                data = allocator.allocate(dimsProd); 
                createPtr();
                if constexpr (ordering == ROW_MAJOR_ORDER){
                    T *datafollower = data;
                    for(auto it1 = values.begin(); it1 != values.end(); it1++){
                        for(auto it2 = it1->begin(); it2 != it1->end(); it2++, datafollower++){
                            *datafollower = *it2;
                        }
                    }
                } else {
                    int i = 0;
                    for(auto it1 = values.begin(); it1 != values.end(); it1++, i++){
                        int j = 0;
                        for(auto it2 = it1->begin(); it2 != it1->end(); it2++, j++){
                            ptr[j][i] = *it2;
                        }
                    }
                }
                
            }

            /**
             * @brief Copy another matrix
             * 
             * @param other the other matrix to copy
             */
            Matrix(const Matrix<T, IDX_TYPE, ordering> &other) : allocator(other.allocator), m(other.m), n(other.n), dimsProd(other.dimsProd) {
                data = allocator.allocate(dimsProd);
                auto &rm = umpire::ResourceManager::getInstance();
                rm.copy(data, other.data);
                createPtr();
            }

            /**
             * @brief move constructor
             * 
             * @param other the other to move
             */
            Matrix(Matrix<T, IDX_TYPE, ordering> &&other) : m(other.m), n(other.n), dimsProd(other.dimsProd), allocator(other.allocator) {
                data = other.data;
                other.data = nullptr;
                ptr = other.ptr;
                other.ptr = nullptr;
            }

            /**
             * @brief Copy assignment
             * 
             * @param other the other to copy
             * @return Matrix<T>& a reference to this
             */
            Matrix<T, IDX_TYPE, ordering>& operator=(const Matrix<T, IDX_TYPE, ordering> &other){
                if(&other != this){
                    m = other.m;
                    n = other.n;
                    dimsProd = other.dimsProd;
                    allocator.deallocate(data, dimsProd);
                    data = nullptr;
                    data = allocator.allocate(dimsProd);
                    auto &rm = umpire::ResourceManager::getInstance();
                    rm.copy(data, other.data);
                    createPtr();
                }
                return *this;
            }

            Matrix<T> &operator=(Matrix<T> &&other){
                if(&other != this){
                    std::swap(this->data, other.data);
                    other.data = nullptr;
                    std::swap(this->ptr, other.ptr);
                    other.ptr = nullptr;
                    allocator = other.allocator;
                    IDX_TYPE mtemp = m;
                    IDX_TYPE ntemp = n;
                    m = other.m;
                    n = other.n;
                    other.m = mtemp;
                    other.n = ntemp;
                    IDX_TYPE dimsProdtemp = dimsProd;
                    dimsProd = other.dimsProd;
                    other.dimsProd = dimsProdtemp;
                }
                return *this;
            }

            /**
             * @brief Destroy the Matrix< T> object
             */
            ~Matrix(){
                allocator.deallocate(data, dimsProd);
                delete[] ptr;
            }

            /**
             * @brief Fill with zeros
             */
            void zeroFill(){
                auto &rm = umpire::ResourceManager::getInstance();
                rm.memset(data, 0);
            }

            /**
             * @brief Set to the Identity Matrix
             */
            void identityFill(){
                zeroFill();
                for(IDX_TYPE i = 0; i < std::min(m, n); i++) ptr[i][i] = 1;
            }

            // ========================
            // = Optimized Operations =
            // ========================

            /**
             * @brief Matrix-vector multiply
             * uses RAJA::simd_exec for SIMD vectorization
             * y = alpha * A *x + beta * y
             * @param [in] alpha the multiplier for Ax
             * @param [in] x the vector to multiply by the matrix and alpha
             * @param [in] beta the multiplier for y
             * @param [in / out] y the vector y in the above equation, where result is stored
             */
            void gemv(T alpha, const T *x, T beta, T* y) const{
                RAJA::View<T, RAJA::Layout<2, IDX_TYPE> > matview(data, m, n);
                int i;
                for(i = 0; i < m; i++) y[i] *= beta;
                using GEMV_EXEC_POL =
                RAJA::KernelPolicy<
                    RAJA::statement::For<1, RAJA::loop_exec,
                        RAJA::statement::For<0, RAJA::simd_exec,
                            RAJA::statement::Lambda<0>
                        >
                    >
                >;
                RAJA::kernel<GEMV_EXEC_POL>(
                    RAJA::make_tuple( 
                        RAJA::TypedRangeSegment<IDX_TYPE>(0, m),
                        RAJA::TypedRangeSegment<IDX_TYPE>(0, n)
                    ),

                    [=] (int i, int j) {
                        y[i] += alpha * matview(i, j) * x[j];
                    }
                );
            }

            /**
             * @brief Matrix-vector multiply Transpose
             * uses RAJA::simd_exec for SIMD vectorization
             * y = alpha * A^T *x + beta * y
             * @param [in] alpha the multiplier for Ax
             * @param [in] x the vector to multiply by the matrix and alpha
             * @param [in] beta the multiplier for y
             * @param [in / out] y the vector y in the above equation, where result is stored
             */
            void gemvT(T alpha, const T *x, T beta, T* y) const{
                RAJA::View<T, RAJA::Layout<2, IDX_TYPE> > matview(data, m, n);
                int j;
                for(j = 0; j < n; j++) y[j] *= beta;

                using GEMV_EXEC_POL =
                RAJA::KernelPolicy<
                    RAJA::statement::For<1, RAJA::loop_exec,
                        RAJA::statement::For<0, RAJA::simd_exec,
                            RAJA::statement::Lambda<0>
                        >
                    >
                >;
                RAJA::kernel<GEMV_EXEC_POL>(
                    RAJA::make_tuple( 
                        RAJA::TypedRangeSegment<IDX_TYPE>(0, m),
                        RAJA::TypedRangeSegment<IDX_TYPE>(0, n)
                    ),

                    [=] (int i, int j) {
                        y[j] += alpha * matview(i, j) * x[i];
                    }
                );
            }

/*
            void gemm(
                CBLAS_TRANSPOSE transa,
                CBLAS_TRANSPOSE transb,
                T alpha,
                Matrix<T, IDX_TYPE, ordering> &b,
                T beta,
                Matrix<T, IDX_TYPE, ordering> &c
            ){
                if constexpr(std::is_same<T, double>::value){
                    if constexpr(ordering == ORDERING::ROW_MAJOR_ORDER){
                        cblas_dgemm(CblasRowMajor, transa, transb, m, b.getN(), n, alpha, data, n, b.data, b.getN(), beta, c.data, c.getN());
                    } else {
                        cblas_dgemm(CblasColMajor, transa, transb, m, b.getN(), n, alpha, data, n, b.data, b.getN(), beta, c.data, c.getN());
                    }
                } else if constexpr(std::is_same<T, float>::value){
                    if constexpr(ordering == ORDERING::ROW_MAJOR_ORDER){
                        cblas_sgemm(CblasRowMajor, transa, transb, m, b.getN(), n, alpha, data, n, b.data, b.getN(), beta, c.data, c.getN());
                    } else {
                        cblas_sgemm(CblasColMajor, transa, transb, m, b.getN(), n, alpha, data, n, b.data, b.getN(), beta, c.data, c.getN());
                    }
                }
            }

*/

            // ===============================
            // = Linear Operator Definitions =
            // ===============================

            /**
             * @brief Apply this linear operator to a vector x
             * 
             * @param x the vector to apply this to
             * @param out the result
             */
            void mult(const T *x, T *out) const{
                gemv(1.0, x, 0.0, out);
            }

            void mult(const VectorConcept auto& x, VectorConcept auto& out) const{
                // TODO: FIX FOR COLUMN MAJOR ORDER
                for(int j = 0; j < m; j++) out[j] = 0;
                T *datafollower = data;
                for(IDX_TYPE i = 0; i < m; i++){
                    for(int j = 0; j < n; j++, datafollower++){
                        out[i] +=  *datafollower * x[j];
                    }
                }
            }

            void TransposeMultiply(const VectorConcept auto& x, VectorConcept auto& out) const {
                for(int j = 0; j < n; j++) out[j] = 0;
                if constexpr (ordering == ROW_MAJOR_ORDER){
                    for(int i = 0; i < m; i++){
                        for(int j = 0; j < n; j++){
                            out[j] += x[i] * ptr[i][j]; // most efficient memory access
                        }
                    }
                } else {

                }
            }

            void TransposeMultiply(const Vector<T, IDX_TYPE> &x, Vector<T, IDX_TYPE> &out) const {
                for(int j = 0; j < n; j++) out[j] = 0;
                if constexpr (ordering == ROW_MAJOR_ORDER){
                    for(int i = 0; i < m; i++){
                        for(int j = 0; j < n; j++){
                            out[j] += x[i] * ptr[i][j]; // most efficient memory access
                        }
                    }
                } else {

                }
            }

            /**
             * @brief Fully optimized AT * A * x
             * 
             * @param x the vector input
             * @param out the output
             */
            void ATAx(const T *x, T *out) const{
                for(int j = 0; j < n; j++) out[j] = 0;
                if constexpr(ordering == ROW_MAJOR_ORDER){
                    for(int i = 0; i < m; i++){
                        for(int j = 0; j < n; j++){
                            for(int k = 0; k < n; k++){
                                out[j] += ptr[i][j] * ptr[i][k] * x[k];
                            }
                        }
                    }
                } else {
                    // TODO
                }
            }

            /**
             * @brief Perform the matrix inner product
             * <x, Ax>
             * WARNING: bounds of x cannot be checked with IndexableVector
             * A must be a square matrix with the same size as x
             * @param x the vector to perform the inner product with
             * @return T the result of the inner product
             */
            T matInnerProd(const IndexableVector auto& x) const {
                T result = 0;
                if(m != n) ERR::throwError("Inner product requires square matrix.");
                // note j and i are canonically correct for column major order
                // but the algorithm applies generally
                for(IDX_TYPE j = 0; j < m; j++){ 
                    T prod = 0;
                    for(IDX_TYPE i = 0; i < n; i++){
                        prod += ptr[j][i] * x[i];
                    }
                    result += prod * x[j];
                }
                return result;
            }

            T matInnerProd(const T *x) const {
                T result = 0;
                if(m != n) ERR::throwError("Inner product requires square matrix.");
                // note j and i are canonically correct for column major order
                // but the algorithm applies generally
                for(IDX_TYPE j = 0; j < m; j++){ 
                    T prod = 0;
                    for(IDX_TYPE i = 0; i < n; i++){
                        prod += ptr[j][i] * x[i];
                    }
                    result += prod * x[j];
                }
                return result;
            }

            /**
             * @brief Get the ordering of the matrix for non-templated use
             * 
             * @return ORDERING the ordering of the matrix
             */
            ORDERING getOrdering(){
                return ordering;
            }

            /**
             * @brief index into the matrix
             * 
             * @param idx the row number to get
             * @return double* the pointer to the start of that row
             */
            typename MatrixReturnType<T, IDX_TYPE, ordering>::type operator[](IDX_TYPE i){
                if constexpr (ordering == ROW_MAJOR_ORDER)
                    return ptr[i];
                else {
                    return MatrixIndexProxy<T, IDX_TYPE>(ptr, i);
                }
            }

            /**
             * @brief index into the matrix
             * 
             * @param idx the row number to get
             * @return double* the pointer to the start of that row
             */
            const typename MatrixReturnType<T, IDX_TYPE, ordering>::type operator[](IDX_TYPE i) const{
                if constexpr (ordering == ROW_MAJOR_ORDER)
                    return ptr[i];
                else {
                    return MatrixIndexProxy<T, IDX_TYPE>(ptr, i);
                }
            }

            /**
             * @brief Multiply the matrix by a scalar alpha
             * 
             * @param alpha the scalar
             * @return Matrix<T, IDX_TYPE, ordering>& a reference to the result
             */
            const Matrix<T, IDX_TYPE, ordering> &operator *=(T alpha){
                for(T *it = data; it != data + dimsProd; it++) *it *= alpha;
                return *this;
            }


            /**
             * @brief A = alpha * A + beta * B
             * 
             * @param alpha the first coefficient
             * @param beta the second coefficient
             * @param B the other matrix
             * @return a reference ot the result
             */
            const Matrix<T, IDX_TYPE, ordering> &addScaled(T alpha, T beta, Matrix<T, IDX_TYPE, ordering> &B){
                T *it2 = B.data;
                for(T *it = data; it != data + dimsProd; it++, it2++) *it = alpha * *it + beta * *it2;
                return *this;
            }
            
            /**
             * @brief get the number of rows
             * 
             * @return const IDX_TYPE 
             */
            const IDX_TYPE getM() const{ return m; }

            /**
             * @brief get the number of columns
             * 
             * @return const IDX_TYPE 
             */
            const IDX_TYPE getN() const{ return n; }

            /**
             * @brief Print the matrix to the given output stream
             * @param out the output stream
             */
            void prettyPrint(std::ostream &out){
                out << std::setprecision(4);
                for(IDX_TYPE i = 0; i < m; i++){
                    for(IDX_TYPE j = 0; j < n; j++){
                        out << std::setw(12) << this->operator[](i)[j];
                    }
                    out << "\n";
                }
            }
        };


        template<typename T, typename IDX_TYPE=std::size_t>
        class Vector {
            public:
            MEMORY::data_pointer<T> data;
            IDX_TYPE sz;

            /**
             * @brief Construct an uninitialized vector
             * 
             */
            Vector(): sz(0), data((std::size_t) 0) {}

            void init(IDX_TYPE sz){
                data.resize(sz);
                this->sz = sz;    
            }

            /**
             * @brief Construct a new Vector object from an existing pointer
             * 
             * @param existing the existing pointer
             * @param sz the size
             */
            Vector(T *existing, IDX_TYPE sz): data(existing), sz(sz) {}

            Vector(IDX_TYPE sz) : sz(sz), data(sz) {};

            Vector(const Vector<T, IDX_TYPE> &other) : sz(other.sz), data(other.sz) {
                if constexpr (std::is_fundamental<T>::value){
                    memcpy(data, other.data, sz * sizeof(T));
                } else {
                    std::copy(other.data, other.data + sz, data);
                }
            }

            Vector(Vector<T, IDX_TYPE> &&other) : sz(other.sz){
                data = std::move(other.data);
            }

            Vector<T, IDX_TYPE>& operator=(const Vector<T, IDX_TYPE> &other){
                if(&other != this){
                    if(other.sz != sz){ // TODO: check this
                        sz = other.sz;
                        data.resize(sz);
                    }
                    if constexpr (std::is_fundamental<T>::value){
                        memcpy(data, other.data, sz * sizeof(T));
                    } else {// TODO: check this memmove might not be right for this
                        std::copy(other.data, other.data + sz, data);
                    }
                }
                return *this;
            }

            Vector<T, IDX_TYPE>& operator=(Vector<T> &&other){
                if(&other != this){
                    sz = other.sz;
                    data = std::move(other.data);
                }
                return *this;
            }

            Vector<T, IDX_TYPE> &operator +=(Vector<T, IDX_TYPE> &other){
                for(IDX_TYPE i = 0; i < sz; i++){
                    data[i] += other[i];
                }
                return *this;
            }

            Vector<T, IDX_TYPE> &operator -=(Vector<T, IDX_TYPE> &other){
                for(IDX_TYPE i = 0; i < sz; i++){
                    data[i] -= other[i];
                }
                return *this;
            }

            T &operator[](std::ptrdiff_t idx){
                return data[idx];
            }

            const T &operator[](std::ptrdiff_t idx) const{
                return data[idx];
            }

            /**
             * @brief Multiply by a scalar
             * @param alpha the scalar
             * 
             * @return a reference to this
             */
            Vector<T, IDX_TYPE> & operator*=(T alpha){
                for(T *it = data; it != data + sz; it++) *it *= alpha;
                return *this;
            }

            /**
             * @brief Add another vector to this one
             * Assumes same sz vectors
             * @param other the other vector to add
             * @return Vector<T>& a reference to this
             */
            Vector<T, IDX_TYPE> & operator +=(const Vector<T, IDX_TYPE> &other){
                for(int i = 0; i < sz; i++) data[i] += other.data[i];
                return *this;
            }

            Vector<T, IDX_TYPE> operator-() const {
                Vector<T, IDX_TYPE> neg(sz);
                for(IDX_TYPE i = 0; i < sz; ++i){
                    neg[i] = -data[i];
                }
                return neg;
            }

            /**
             * @brief add another vector to this scaled by a scalar
             * 
             * @param other the other vector
             * @param alpha the multiplier scalar
             * @return const Vector<T>& this
             */
            const Vector<T, IDX_TYPE> &addScaled(const Vector<T, IDX_TYPE> &other, T alpha){
                for(int i = 0; i < sz; i++) data[i] += other.data[i] * alpha;
                return *this;
            }

            /**
             * @brief Get the dot product of two vectors
             * 
             * @param other the other vector to get the dot product with
             * @return T the dot product
             */
            T dot(const Vector<T, IDX_TYPE> &other){
                T sum = ZERO<T>;
                for(int i = 0; i < sz; i++) sum += data[i] * other.data[i];
                return sum;
            }

            /**
             * @brief Get the Euclidian norm
             * 
             * @return T the Euclidian norm
             */
            T norm(){
                T sum = ZERO<T>;
                for(int i = 0; i < sz; i++) sum += data[i] * data[i];
                return std::sqrt(sum);
            }

            /**
             * @brief Get the length of the vector
             * 
             * @return IDX_TYPE the 
             */
            IDX_TYPE size(){
                return sz;
            }

            /**
             * @brief Fill with zeros
             */
            void zeroFill(){
                for(T *it = data; it != data + sz; it++) *it = ZERO<T>;
            }

            void resize(IDX_TYPE n){
                T *datanew = new T[n];
                for(int i = 0; i < std::min(n, sz); i++) datanew[i] = data[i];
                data = datanew;
                sz = n;
            }

            /**
             * @brief copies this sz of values from the argument
             * 
             * @param values the values to copy
             */
            void copyArray(T *values){
                for(int i = 0; i < sz; i++) data[i] = values[i];
            }

            /**
             * @brief Print the matrix to the given output stream
             * @param out the output stream
             */
            void prettyPrint(std::ostream &out){
                out << "[";
                out << std::setprecision(4);
                for(IDX_TYPE j = 0; j < sz; j++){
                    out << std::setw(12) << this->operator[](j);
                }
                out << " ";
                out << "]\n";
            }
        };

        /**
         * @brief An integral type vector that represents permutations of the major dimension of a matrix
         * 
         * @tparam IDX the integral indexing type
         */
        template<typename IDX = std::size_t>
        class PermutationMatrix{
            public:
            IDX size;
            IDX *data;

            /**
             * @brief Construct a new Permutation Matrix object
             * starts as the identity
             * @param size the size of the permutation matrix
             */
            PermutationMatrix(IDX size): size{size}, data(new IDX[size]) {
                for(IDX i = 0; i < size; i++) data[i] = i;
            }

            /** @brief construct and empty permutation matrix */
            PermutationMatrix() : PermutationMatrix(0) {}

            /**
             * @brief Move Constructor
             * 
             * @param other the other permutation matrix
             */
            PermutationMatrix(PermutationMatrix<IDX> &&other) : size{other.size}, data{other.data}{
                other.data = nullptr;
                other.size = 0;
            }

            /**
             * @brief Copy Constructor
             * 
             * @param other the other permutation matrix to copy
             */
            PermutationMatrix(PermutationMatrix<IDX> &other):size{other.size}, data(new IDX[size]){
                for(IDX i = 0; i < size; i++){
                    data[i] = other.data[i];
                }
            }

            PermutationMatrix<IDX> &operator=(PermutationMatrix<IDX> &&other) {
                size = other.size;
                data = other.data;
                other.size = 0;
                other.data = nullptr;
                return *this;
            }

            PermutationMatrix<IDX> &operator=(PermutationMatrix<IDX> &other){
                size = other.size;
                delete[] data;
                data = new IDX[size];
                for(IDX i = 0; i < size; i++){
                    data[i] = other.data[i];
                }
                return *this;
            }
        
            /**
             * @brief index into the permutation matrix to get the row number
             * 
             * @param row the original row number
             * @return IDX& the permuted row number
             */
            IDX &operator[](IDX row){
                return data[row];
            }

            /**
             * @brief swap two rows
             * 
             * @param i the index of the first row
             * @param j the index of the second row
             */
            void swap(IDX i, IDX j){
                std::swap(data[i], data[j]);
            }

            /**
             * @brief apply the permutation to the matrix
             * 
             * O(n)
             * 
             * @tparam T The floating point type of the matrix
             * @param mat the matrix to permute
             */
            template<typename T>
            void apply(Matrix<T, IDX> &mat){
                IDX matsize = (mat.getOrdering() == ROW_MAJOR_ORDER) ? mat.m : mat.n;
                for(IDX i = 0; i < matsize; i++){
                    if(data[i] > i){
                        std::swap(mat.ptr[data[i]], mat.ptr[i]);
                    }
                }
            }
        };

        template<typename T>
        void MatxVec(const Matrix<T> &mat, const Vector<T> &vec, Vector<T> &result){
            result.zeroFill();
            for(int i = 0; i < mat.m; i++){
                for(int j = 0; j < mat.n; j++){
                    result[j] += mat[i][j] * vec[j];
                }
            }
        }

        /**
         * @brief Multiply a matrix and a vector
         * 
         * @tparam T the floating point type
         * @param mat the matrix
         * @param vec the vector
         * @param result the result
         */
        template<typename T>
        void MatxVec(const Matrix<T> &mat, const T *vec, T *result){
            for(int i = 0; i < mat.n; i++) result[i] = 0;
            for(int i = 0; i < mat.m; i++) {
                for(int j = 0; j < mat.n; j++) result[j] += mat[i][j] * vec[j];
            }
        }

        /**
         * @brief Gets the adjugate of A for 1x1, 2x2, or 3x3 matrix
         * 
         * @tparam matsize the matrix size
         * @tparam T the floating point type
         * @param A the matrix
         * @param adj [out] the adjugate (must be presized)
         */
        template<std::size_t matsize, typename T>
        void adjugate(const TENSOR::Matrix<T> &A, TENSOR::Matrix<T> &adj){
            if constexpr (matsize == 1){
                adj[0][0] = 1;
            } else if constexpr(matsize == 2){
                adj[0][0] = A[1][1];
                adj[0][1] = -A[0][1];

                adj[1][0] = -A[1][0];
                adj[1][1] = A[0][0];
            } else {
                adj[0][0] = A[1][1] * A[2][2] - A[1][2] * A[2][1];
                adj[0][1] = A[0][2] * A[2][1] - A[0][1] * A[2][2];
                adj[0][2] = A[0][1] * A[1][2] - A[1][1] * A[0][2];

                adj[1][0] = A[1][2] * A[2][0] - A[1][0] * A[2][2];
                adj[1][1] = A[0][0] * A[2][2] - A[0][2] * A[2][0];
                adj[1][0] = A[0][1] * A[2][0] - A[0][0] * A[2][1];

                adj[2][0] = A[0][1] * A[1][2] - A[0][2] * A[1][1];
                adj[2][1] = A[0][1] * A[2][0] - A[0][0] * A[2][1];
                adj[2][2] = A[0][0] * A[1][1] - A[0][1] * A[1][0];
            }
        }
    }
}

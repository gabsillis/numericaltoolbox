/**
 * @file directSolvers.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Direct Linear Solvers and Factorizations
 * @version 0.1
 * @date 2021-09-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <Numtool/matrix/matrix.hpp>
#include <concepts>
#include <limits>
namespace MATH::TENSOR{

    /**
     * @brief Decomposes the matrix A into LU factors
     * creates the permuation matrix with partial pivoting
     * 
     * @tparam T The flaoting point type
     * @tparam IDX the index type
     * @param A [in/out] The matrix to decompose result is stored in A
     * @param pi [out] the permutation matrix (unused)
     */
    template<typename T, typename IDX=std::size_t>
    void LUDecompose(Matrix<T, IDX> &A, PermutationMatrix<IDX> &pi){
        T max;
        IDX maxidx;
        // naive algorithm
        for(IDX j = 0; j < A.n; j++){
            // pivoting
            max = std::abs(A[j][j]);
            maxidx = j;
            for(int i = j + 1; i < A.m; i++){
                T val = std::abs(A[i][j]);
                if(val > max){
                    max = val;
                    maxidx = i;
                }
            }

            if(std::abs(max) < 2 * std::numeric_limits<T>::epsilon()){
                ERR::throwWarning("Matrix is singular to working precision.");
                return;
            }

            pi.swap(maxidx, j);
            std::swap(A.ptr[maxidx], A.ptr[j]);

            // decomposition step
            for(IDX i = j + 1; i < A.m; i++){
                A[i][j] /= A[j][j]; // multipliers
                for(IDX k = j + 1; k < A.n; k++){
                    A[i][k] -=  A[i][j] * A[j][k];
                }
            }
        }
    }

    /**
     * @brief forward substitution
     * 
     * @tparam T - floating point
     * @tparam IDX - index type
     * @param A - LU decomposed matrix
     * @param pi - permutation matrix
     * @param b - RHS vector
     * @param y - solution of Ly=b
     */
    template<typename T, typename IDX=std::size_t>
    void FWDSub(Matrix<T, IDX> &A, PermutationMatrix<IDX> &pi, T*b, T*y){
        for(IDX i=0;i<A.m;i++){
            y[i] = b[pi[i]];
            for(IDX j=0;j<i;j++){
                y[i]-=A[i][j] * y[j];
            }
        }
    }

    template<typename T, typename IDX=std::size_t>
    void backsub(Matrix<T, IDX> &A, T*y, T*x) requires std::integral<IDX>{
        for(IDX i=A.m-1; i != (IDX) -1; i--){ // SAFE FOR UNSIGNED INTS (STOPS AT 0)
            x[i] = y[i];
            for(IDX k=i+1; k<A.n; k++){
                x[i] -= A[i][k] * x[k];
            }
            x[i] /= A[i][i];
        }
    }
}

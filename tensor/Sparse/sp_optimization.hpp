/**
 * @file optimization.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Sparse optimization Kernel methods
 * @version 0.1
 * @date 2021-07-24
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <cstddef>

namespace MATH::TENSOR::SPARSE {

    /**
     * @brief Compute the Levenberg Marquardt step from a QR factorization of the Jacobian
     * For column major compressed column storage (CCS)
     * 
     * @tparam T the floating point type
     * @tparam SZ_INT the size integer type
     * @tparam IDX_INT the indexing integer type
     * @param m the number of rows in the matrix
     * @param n the number of columns in the matrix
     * @param R the right trapezoidal part of the QR decomposition of the Jacobian in CCS
     * @param Ridx the row indices for the entries in R
     * @param Rcolstart the pointers to the start of each column in R
     * @param D The diagonal of the jacobian (this is multiplied by the sqrt(lambda) in the ellipsoidal LM algorithm)
     *          Use the Identity for classical LM
     * @param lambda the trust region radius
     * @param Qtf the result of Q transpose times f compute this before using this method
     * @param p [out] the step
     * @param sdiag [out] the diagonal of S from p^T* (AA^T + DD) * p = S^T S
     */
    template<typename T, typename SZ_INT, typename IDX_INT>
    void colmaj_lm_rdiag_step(
        SZ_INT m,
        SZ_INT n,
        T *R,
        IDX_INT *Ridx,
        IDX_INT *Rcolstart,
        const T *D,
        T lambda,
        const T* Qtf,
        T *p,
        T *sdiag
    );
}
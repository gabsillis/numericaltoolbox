#include <Numtool/matrix/matrix.hpp> 
#include <Numtool/matrix/solvers/directSolvers.hpp>
#include <iostream>

int main(){
    using namespace MATH::TENSOR;
    int i, j;
    std::cin >> i >> j;
    Matrix<double, int> A(i, j);
    
    std::cin >> A;
    PermutationMatrix<int> pi{i}; 
    std::vector<double> b(j);
    std::vector<double> y(j);
    std::vector<double> x(j);

    double fill;
    for(int k = 0; k < j; ++k) std::cin >> fill >> fill >> b[k];
    LUDecompose(A, pi);
    std::cout << A;

    FWDSub(A, pi, b.data(), y.data());

    std::cout << "y: \n";
    for(int k = 0; k < i; ++k) std::cout << y[k] << "\n";

    backsub(A, y.data(), x.data());
    std::cout << "\n\n";
    for(int k = 0; k < j; ++k){
        std::cout << x[k] << "\n";
    }

    std::cout << "\n\n";
    FWDSub(A, pi, b.data(), x.data());
    backsub(A, x.data(), x.data());
    std::cout << "\n\n";
    for(int k = 0; k < j; ++k){
        std::cout << x[k] << "\n";
    }

}

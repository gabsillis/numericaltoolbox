/**
 * @file tuple_utils.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief utilities for working with tuples
 * @version 0.1
 * @date 2022-08-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
#include <tuple>
namespace MATH {
    namespace TMP {

        // ===============
        // = Sized Tuple =
        // ===============
        template<std::size_t size, typename type, typename... tuple_args>
        struct sized_tuple {
            using tuple_type = typename sized_tuple<size - 1, type, type, tuple_args...>::tuple_type;
        };

        template<typename arg1, typename... tuple_args>
        struct sized_tuple<1, arg1, tuple_args...> {
            using tuple_type = std::tuple<tuple_args...>;
        };

        template<typename type>
        struct sized_tuple<0, type> {
            using tuple_type = std::tuple<>;
        };

        template<std::size_t size, typename type>
        inline typename sized_tuple<size, type>::tuple_type fill_sized_tuple(type arg){
            
        }

        template<std::size_t size, typename type>
        void fill_sized_tuple_helper(type arg, typename sized_tuple<size, type>::tuple_type &tuple){
            std::get<size - 1>(tuple) = arg;
            if constexpr(size > 1){
                fill_sized_tuple_helper<size - 1, type>(arg, tuple);
            }
        }
    }
}

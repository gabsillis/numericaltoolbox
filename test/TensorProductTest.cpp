#include <gtest/gtest.h>
#include <Numtool/tensor.hpp>
#include <string>
#include <vector>

using namespace MATH::TENSOR_PROD;

struct TestSymbol {
    std::string val = "";

    TestSymbol() = default;
    TestSymbol(const TestSymbol &other) = default;
    TestSymbol(TestSymbol &&other) = default;

    TestSymbol &operator=(const TestSymbol &other) = default;
    TestSymbol &operator=(TestSymbol &&other) = default;


    TestSymbol(std::string val) : val(val) {}

    TestSymbol &operator*=(const TestSymbol &other){
        val = "(" + val + "*" + other.val + ")";
        return *this;
    }

};

TestSymbol operator*(const TestSymbol &val1, const TestSymbol &val2){
    return TestSymbol{"(" + val1.val + "*" + val2.val + ")"};
}

struct TestFunction {
    TestSymbol eval(int Pn, TestSymbol xi){
        return TestSymbol{"fcn" + std::to_string(Pn) + "(" + xi.val + ")"};
    }

    TestSymbol derivative(int Pn, TestSymbol xi){
        return TestSymbol{"df" + std::to_string(Pn) + "(" + xi.val + ")"};
    }

    TestSymbol derivative2nd(int ibasis, TestSymbol xi){ return jthDerivative(ibasis, 2, xi); }

    TestSymbol jthDerivative(int ibasis, int jderivative, TestSymbol xi){
        return TestSymbol{"d" + std::to_string(jderivative) + "f" + std::to_string(ibasis) + "(" + xi.val + ")"};
    }
}; 

TEST(TestTensorProd, TestLinf){
    using T = TestSymbol;
    LinfTensorProduct<TestSymbol, TestFunction, 3, 3> tensprod{};
    constexpr int ndim = 3;
    T grad[ndim];
    T hess[ndim][ndim];
    std::string x1 = "x";
    std::string x2 = "y";
    std::string x3 = "z";
    T xi[ndim] = {x1, x2, x3};

    T a = tensprod.getProduct(xi, 2);
    tensprod.getGradient(xi, 2, grad);
    tensprod.getHessian(xi, 2, hess);
}


#include <gtest/gtest.h>
#include <Numtool/tensor.hpp>
#include <Numtool/utilities/CRSStorage.hpp>

TEST(TestTensor, TestInit){
    using namespace MATH::TENSOR;
    Tensor<double, 3> T3{4, 5, 2};

    ASSERT_EQ(T3.totalSize(), 40);
    ASSERT_EQ(T3.dimSize(0), 4);
    ASSERT_EQ(T3.dimSize(1), 5);
    ASSERT_EQ(T3.dimSize(2), 2);
}

TEST(TestTensor, TestIndex){
    using namespace MATH::TENSOR;
    Tensor<double, 3> T3{4, 5, 2};

    T3[0][0][0] = 4;
    ASSERT_EQ(T3(0, 0, 0), 4);
    T3[3][4][1] = 16;
    ASSERT_EQ(T3(3, 4, 1), 16);
}

TEST(TestMatrix, TestInit){
    using namespace MATH::TENSOR;
    Matrix<double, int, ORDERING::ROW_MAJOR_ORDER> mat1(3, 4);
    ASSERT_EQ(mat1.getM(), 3);
    ASSERT_EQ(mat1.getN(), 4);

    Matrix<double, int, ROW_MAJOR_ORDER> mat2{{3.0, 4.0, 2.0}, {2.0, 3.1, 5.2}};
    ASSERT_EQ(mat2.getM(), 2);
    ASSERT_EQ(mat2.getN(), 3);
    ASSERT_EQ(mat2[0][0], 3.0);
    ASSERT_EQ(mat2[1][1], 3.1);
    ASSERT_EQ(mat2[1][2], 5.2);

    Matrix<double, int, COLUMN_MAJOR_ORDER> mat3{{3.0, 4.0, 2.0}, {2.0, 3.1, 5.2}};
    ASSERT_EQ(mat3.getM(), 2);
    ASSERT_EQ(mat3.getN(), 3);
    ASSERT_EQ(mat3[0][0], 3.0);
    ASSERT_EQ(mat3[1][1], 3.1);
    ASSERT_EQ(mat3[1][2], 5.2);
}

TEST(TestCRS, TestConstruct){
    using namespace MATH::TENSOR;
    CRS<int> crs1{{11, 12, 13}, {21, 22}, {31}, {41, 42, 43}};
    ASSERT_EQ(crs1.data[0], 11);
    ASSERT_EQ(crs1.data[1], 12);
    ASSERT_EQ(crs1.data[2], 13);

    ASSERT_EQ(crs1.data[3], 21);
    ASSERT_EQ(crs1.data[4], 22);

    ASSERT_EQ(crs1.data[5], 31);

    ASSERT_EQ(crs1.data[6], 41);
    ASSERT_EQ(crs1.data[7], 42);
    ASSERT_EQ(crs1.data[8], 43);

    CRS<int> crs2{crs1};
    ASSERT_EQ(crs2.data[0], 11);
    ASSERT_EQ(crs2.data[1], 12);
    ASSERT_EQ(crs2.data[2], 13);

    ASSERT_EQ(crs2.data[3], 21);
    ASSERT_EQ(crs2.data[4], 22);

    ASSERT_EQ(crs2.data[5], 31);

    ASSERT_EQ(crs2.data[6], 41);
    ASSERT_EQ(crs2.data[7], 42);
    ASSERT_EQ(crs2.data[8], 43);

    CRS<int> crs3{std::move(crs1)};
    ASSERT_EQ(crs3.data[0], 11);
    ASSERT_EQ(crs3.data[1], 12);
    ASSERT_EQ(crs3.data[2], 13);

    ASSERT_EQ(crs3.data[3], 21);
    ASSERT_EQ(crs3.data[4], 22);

    ASSERT_EQ(crs3.data[5], 31);

    ASSERT_EQ(crs3.data[6], 41);
    ASSERT_EQ(crs3.data[7], 42);
    ASSERT_EQ(crs3.data[8], 43);
}

TEST(TestCRS, TestIndexing){
    using namespace MATH::TENSOR;
    CRS<int> crs{{11, 12, 13}, {21, 22}, {31}, {41, 42, 43}};

    ASSERT_EQ(crs.rowSize(0), 3);
    ASSERT_EQ(crs.rowSize(1), 2);
    ASSERT_EQ(crs.rowSize(2), 1);
    ASSERT_EQ(crs.rowSize(3), 3);

    ASSERT_EQ(*crs.start(0), 11);
    ASSERT_EQ(*(crs.end(3) - 1), 43);

    ASSERT_EQ(crs[0][0], 11);
    ASSERT_EQ(crs[0][2], 13);

    ASSERT_EQ(crs[3][2], 43);
}

TEST(TestCRS, TestMoveAssignment){
    using namespace MATH::TENSOR;
    CRS<int> crs{{11, 12, 13}, {21, 22}, {31}, {41, 42, 43}};

    CRS<int> crs2{{}};
    crs2 = std::move(crs);

    ASSERT_EQ(crs2.data[0], 11);
    ASSERT_EQ(crs2.data[1], 12);
    ASSERT_EQ(crs2.data[2], 13);

    ASSERT_EQ(crs2.data[3], 21);
    ASSERT_EQ(crs2.data[4], 22);

    ASSERT_EQ(crs2.data[5], 31);

    ASSERT_EQ(crs2.data[6], 41);
    ASSERT_EQ(crs2.data[7], 42);
    ASSERT_EQ(crs2.data[8], 43);
}
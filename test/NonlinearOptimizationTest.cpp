/**
 * @file NonlinearOptimizationTest.cpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Tests for nonlinear optimization
 * @version 0.1
 * @date 2022-01-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <gtest/gtest.h>
#include "testProblems.hpp"
#include "optimization.hpp"
#include "TrustRegion.hpp"
#include "single_objective/single_objective_cg.hpp"
#include "alm.hpp"
#include "cgls.hpp"
#include "newton_linesearch.hpp"
#include <iostream>

class SOCost1 : OPTIMIZATION::SingleObjectiveCost<double, int> {
    using T = double;
    using IDX = int;
    public:
    T eval(T *x) override {
        return SQUARED(x[0]) + SQUARED(x[1]) + x[0] * x[1] - 3 * x[0];
    }

    void applyHessian(T *x, T *b, T *result) override {
        // Q = | 2 1 |
        //     | 1 2 |
        result[0] = 2 * b[0] + b[1];
        result[1] = b[0] + 2 * b[1];
    }
};

TEST(NL_SOLVER_TEST, SINGLE_OBJECTIVE_CG){

}

TEST(NL_SOLVER_TEST, SALM){
    using namespace MATH::TENSOR;
    using namespace OPTIMIZATION;

    Test1<double, int> cost{};
    MATH::TENSOR::Vector<double, int> x(cost.getN());
    x[0] = 1;
    x[1] = 1;
    x[2] = -1;
    x[3] = 0.5;

    SALM<double, int> salm{};
    salm.setLambda0(1e-8);
    std::vector<double> residuals;
    
    salm.solve(cost, x, residuals);
    std::cout << "residuals:";
    for(int i = 0; i < residuals.size(); i++){
        std::cout << residuals[i] << "\n";
    }

    std::cout << "x: [ ";
    for(int i = 0; i < 4; i++) std::cout << x[i] << " ";
    std::cout << "]\n";

    ASSERT_TRUE(residuals[residuals.size() -1] < 1e-8);
    Vector<double, int> r(cost.getN());
    cost.eval(x.data, r.data);
    ASSERT_DOUBLE_EQ(r.norm(), residuals[residuals.size() - 1]);
}

TEST(NL_SOLVER_TEST, GAUSS_NEWTON_LINESEARCH){
    using namespace MATH::TENSOR;
    using namespace OPTIMIZATION;

    Test1<double, int> cost{};
    MATH::TENSOR::Vector<double, int> x(cost.getN());
    x[0] = 1;
    x[1] = 1;
    x[2] = -1;
    x[3] = 0.5;

    GaussNewtonLinesearch<double, int> gn{};

    std::vector<double> residuals;
    
    gn.solve(cost, x, residuals);

    std::cout << "residuals:";
    for(int i = 0; i < residuals.size(); i++){
        std::cout << residuals[i] << "\n";
    }

    std::cout << "x: [ ";
    for(int i = 0; i < 4; i++) std::cout << x[i] << " ";
    std::cout << "]\n";


}

/**
TEST(NL_SOLVER_TEST, CGLS){
    using namespace MATH::TENSOR;
    using namespace OPTIMIZATION;

    Test1<double, int> cost{};
    MATH::TENSOR::Vector<double, int> x(cost.getN());
    x[0] = 1;
    x[1] = 1;
    x[2] = .95;
    x[3] = 1;

    CGLS<double, int> cgls{};

    std::vector<double> residuals;
    
    cgls.solve(cost, x, residuals);
    // std::cout << "residuals:";
    // for(int i = 0; i < residuals.size(); i++){
    //     std::cout << residuals[i] << "\n";
    // }

    // std::cout << "x: [ ";
    // for(int i = 0; i < 4; i++) std::cout << x[i] << " ";
    // std::cout << "]\n";

    // ASSERT_TRUE(residuals[residuals.size() -1] < 1e-8); // different convergence criteria
    Vector<double, int> r(cost.getN());
    cost.eval(x.data, r.data);
    ASSERT_DOUBLE_EQ(r.norm(), residuals[residuals.size() - 1]);
}
*/

TEST(NL_SOLVER_TEST, STEIHAUG_TR){
    using namespace MATH::TENSOR;
    using namespace OPTIMIZATION;

    Test1<double, int> cost{};
    MATH::TENSOR::Vector<double, int> x(cost.getN());
    x[0] = 1;
    x[1] = 1;
    x[2] = .95;
    x[3] = 1;

    

    std::vector<double> residuals;
    
    PRECON::IdentityPreconditioner<double, int> pre{4};
    Steihaug<double, int, TR_OPTIONS::LinearSolver::CG, GNHessianApprox<double, int>> subproblem{pre};
    TrustRegion<double, int, TR_OPTIONS::LinearSolver::CG, TR_OPTIONS::ConvergenceTest::ABSOLUTE_TOLERANCE> trust{&subproblem};
    trust.solve(cost, x, residuals);

    // std::cout << "residuals:";
    // for(int i = 0; i < residuals.size(); i++){
    //     std::cout << residuals[i] << "\n";
    // }

    // std::cout << "x: [ ";
    // for(int i = 0; i < 4; i++) std::cout << x[i] << " ";
    // std::cout << "\n";

    Vector<double, int> r(cost.getM());
    cost.eval(x.data, r.data);
    double r0 = r.norm();

    ASSERT_TRUE(r0 * residuals[residuals.size() -1] < 1e-8);
    cost.eval(x.data, r.data);
    ASSERT_DOUBLE_EQ(r.norm(), residuals[residuals.size() - 1]);
}

// ================================
// = Testing PETSc implementation =
// ================================


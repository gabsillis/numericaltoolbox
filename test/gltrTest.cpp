#include <gtest/gtest.h>
#include "gltr.hpp"
#include "matrix.hpp"
#include "testProblems.hpp"
#include <iostream>

TEST(GLTR_TEST, TESTTRIDIAG){
    using namespace MATH::TENSOR;
    using namespace OPTIMIZATION;

    GLTR_TR<double, int> gltr{};
    gltr.testFactorize();

    Test1<double, int> cost{};
    MATH::TENSOR::Vector<double, int> x(cost.getN());
    x[0] = 1;
    x[1] = 1;
    x[2] = -0.5;
    x[3] = 0.5;
    gltr.setKmax(15);
    gltr.setDelta0(.1);
    gltr.solve(cost, x);

    std::cout << "x [ " << x[0] << ", " << x[1] << ", " << x[2] << ", " << x[3] << " ]\n";
    MATH::TENSOR::Vector<double, int> r(cost.getM());
    cost.eval(x.data, r.data);
    std::cout << "residual: " << r.norm() << "\n";

}
/**
 * @file memory.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Memory Management functions
 * based on Tim Davis utilities
 * @version 0.1
 * @date 2021-07-15
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include <cstdlib>

namespace MEMORY {



    /**
     * @brief Allocate n of a type
     * 
     * @tparam T The type to allocate
     * @param n the number of entries
     * @return T* a pointer to the memory
     */
    template<typename T>
    T *tmalloc(std::size_t n) { return (T *) std::malloc(n * sizeof(T)); }

    /**
     * @brief Allocate n of a type and 
     * initialize the memory to 0
     * 
     * @tparam T The type to allocate
     * @param n the number of entries
     * @return T* a pointer to the memory
     */
    template<typename T>
    T *tcalloc(std::size_t n) { return (T *) std::calloc(n, sizeof(T)); }

    /**
     * @brief Free the memory for a pointer
     * 
     * @tparam T the type
     * @param ptr the pointer
     * @return T* NULL
     */
    template<typename T>
    T *tfree(T *ptr){ if(ptr) std::free(ptr); return NULL; }

    /**
     * @brief Reallocates ptr to a new size
     * 
     * @tparam T The type
     * @param ptr the pointer to reallocate
     * @param n the new size
     * @param ok [out] true if reallocation successful, false otherwise
     * @return T* pointer to the newly allocated memory, or the old memory if it fails
     */
    template<typename T>
    T *trealloc(T *ptr, std::size_t n, bool *ok){
        T *ptrnew;
        ptrnew = (T *) std::realloc(ptr, n * sizeof(T));
        *ok = ptrnew != NULL;
        return *ok ? ptrnew : ptr; 
    }

    /**
     * @brief Register unique ptrs with Scratch Space to have a unified delete call
     * 
     * @tparam T the type
     */
    template<class T>
    class ScratchSpace {

        /**
         * @brief deletes all the objects registered with this scratch space
         * 
         */
        void clear(){}

    };

    /**
     * @brief a pointer to hold data
     * can be assigned a pointer that it doesn't own,
     * or required to create that data and delete when finished
     * @tparam T the type to allocate
     */
    template<typename T>
    class data_pointer {
        private:
        T* data = nullptr;
        T* deleteable = nullptr;

        public:
        data_pointer(){}

        const data_pointer<T>& operator =(const data_pointer<T>& other) = delete;

        const data_pointer<T>& operator =(data_pointer<T>&& other){
            data = other.data;
            deleteable = other.deleteable;
            other.deleteable = nullptr;
            other.data = nullptr;
            return *this;
        }

        data_pointer(const data_pointer<T>&& other) :data(other.data), deleteable(other.deleteable){
            other.data = nullptr;
            other.deleteable = nullptr;
        }

        data_pointer(std::size_t n)
        : data{tmalloc<T>(n)}{ deleteable = data; }

        data_pointer(T *other) : data(other) {}

        operator T* () const {
            return data;
        }

        T* resize(std::size_t n_new){
            if(deleteable == nullptr) return data; // cannot resize unowned pointer
            bool status;
            data = trealloc<T>(data, n_new, &status);
            if(deleteable != nullptr) deleteable = data; // make sure pointer is right
            return data;
        }

// compiler warns ambiguous
//        inline T &operator[](std::size_t i){ return data[i]; }

        ~data_pointer(){ if(deleteable != nullptr) tfree<T>(deleteable); }
    };
}

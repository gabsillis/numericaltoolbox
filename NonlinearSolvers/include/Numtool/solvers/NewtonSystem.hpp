/**
 * @file NewtonSystem.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Newton's method for systems of nonlinear equations
 * @version 0.1
 * @date 2022-02-10
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
#include <Numtool/solvers/NonlinearSolver.hpp>
#include <Numtool/tensor.hpp>

namespace MATH::NONLINEAR {

    template<typename T, typename IDX, MATH::TENSOR::ORDERING matrix_ordering>
    class NewtonSolver final : public DenseNonlinearSolver<T, IDX, matrix_ordering> {
        private:
        T tau_abs = 1e-8;
        T tau_rel = 1e-9;
        T epsilon = 1e-8;

        public:

        SolverStatus<IDX> performIterations(
            IDX kmax,
            DenseResidualFunction<T, IDX, matrix_ordering> &residualFunc,
            T *x,
            std::vector<T> &residuals
        ) override {
            using namespace MATH::TENSOR;
            SolverStatus<IDX> status;
            status.convergenceStatus == STATUS::CONVERGENCE_STATUS::NOT_CONVERGED;

            // get initial residual
            T *r = new T[residualFunc.getN()]();
            T *s = new T[residualFunc.getN()];
            residualFunc.calculateResidual(x, r);
            T r0 = norm<T, IDX, 2>(r, residualFunc.getN());
            residuals.push_back(r0);

            T tau = tau_abs + r0 * tau_rel;
            for(IDX k = 0; k < kmax; k++){
                // to pull out would need nondestructive pivoting
                Matrix<T, IDX, matrix_ordering> jac(residualFunc.getN(), residualFunc.getN());
                // Get Jacobian
                residualFunc.fillJacobian(x, jac);
                //jac.prettyPrint(std::cout);
                // Solve for step and apply step
                //KRYLOV::cg<T, IDX, Matrix<T, IDX, matrix_ordering>>(jac, s, r, epsilon, residualFunc.getN());
                PermutationMatrix<IDX> pi(residualFunc.getN());
                LUDecompose<T, IDX>(jac, pi);
                FWDSub<T, IDX>(jac, pi, r, s);
                backsub<T, IDX>(jac, s, s);

                for(IDX i = 0; i < residualFunc.getN(); i++) x[i] -= s[i];
                // calculate the new residual
                residualFunc.calculateResidual(x, r);
                residuals.push_back(norm<T, IDX, 2>(r, residualFunc.getN()));
                // convergence test
                if(*(residuals.end() - 1) <= tau){
                    status.convergenceStatus = STATUS::CONVERGENCE_STATUS::CONVERGED;
                    status.errorStatus = STATUS::NO_ERROR;
                    status.niter = k + 1;
                    return status;
                }
            }
            status.niter = kmax;
            return status;
        }

        void setTolerance(T tau_abs_arg, T tau_rel_arg, T linearTolerance){
            tau_abs = tau_abs_arg;
            tau_rel = tau_rel_arg;
            epsilon = linearTolerance;
        }

    };

    template<typename T, typename IDX, MATH::TENSOR::ORDERING matrix_ordering>
    class MatrixFreeNewtonSolver final : public DenseNonlinearSolver<T, IDX, matrix_ordering> {
        private:
        T tau_abs = 1e-7;
        T tau_rel = 1e-8;
        T epsilon = 1e-8;

        public:

        SolverStatus<IDX> performIterations(
            IDX kmax,
            DenseResidualFunction<T, IDX, matrix_ordering> &residualFunc,
            T *x,
            std::vector<T> &residuals
        ) override {
            using namespace MATH::TENSOR;
            SolverStatus<IDX> status;
            status.convergenceStatus == STATUS::CONVERGENCE_STATUS::NOT_CONVERGED;

            // get initial residual
            T *r = new T[residualFunc.getN()];
            T *s = new T[residualFunc.getN()];
            residualFunc.calculateResidual(x, r);
            T r0 = norm<T, IDX, 2>(r, residualFunc.getN());
            residuals.push_back(r0);

            // Make matrix free jacobian operator
            MatrixFreeJacobian<T, IDX, matrix_ordering> jac{x, &residualFunc, epsilon};

            T tau = tau_abs + r0 * tau_rel;
            for(IDX k = 0; k < kmax; k++){
                // Solve for step and apply step
                KRYLOV::bicgstab<T, IDX, MatrixFreeJacobian<T, IDX, matrix_ordering>>(
                    jac, s, r, epsilon * norm<T, IDX, 2>(x, residualFunc.getN()), residualFunc.getN());

                for(IDX i = 0; i < residualFunc.getN(); i++) x[i] -= s[i];
                // calculate the new residual
                residualFunc.calculateResidual(x, r);
                residuals.push_back(norm<T, IDX, 2>(r, residualFunc.getN()));
                // convergence test
                if(*(residuals.end() - 1) <= tau){
                    status.convergenceStatus = STATUS::CONVERGENCE_STATUS::CONVERGED;
                    status.errorStatus = STATUS::ERROR_STATE::NO_ERROR;
                    status.niter = k + 1;
                    return status;
                }
            }
            status.niter = kmax;
            status.errorStatus = STATUS::ERROR_STATE::NO_ERROR;
            status.convergenceStatus = STATUS::CONVERGENCE_STATUS::NOT_CONVERGED;
            return status;
            
        }

        void setTolerance(T tau_abs_arg, T tau_rel_arg, T linearTolerance){
            tau_abs = tau_abs_arg;
            tau_rel = tau_rel_arg;
            epsilon = linearTolerance;
        }
    };
}

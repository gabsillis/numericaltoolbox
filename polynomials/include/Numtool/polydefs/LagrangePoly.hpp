/**
 * @file Lagrange.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Lagrange Polynomials
 * @version 0.1
 * @date 2022-01-15
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once
#include <cmath>
#include <array>
#include <Numtool/MathUtils.hpp>
#include <utility>
#include <functional>
#include <iostream>
namespace POLYNOMIAL {

    template<typename T, int Pn>
    struct LagrangeAbscisse {
        std::array<T, Pn + 1> poins = []{
            std::array<T, Pn + 1> ret;
            constexpr int npart = Pn; // number of partitions
            T dxi = 2.0 / npart;
            T xi = -1;
            for(int i = 0; i < Pn + 1; i++) {
                ret[i] = xi;
                xi += dxi;
            }
            return ret;
        }();
    };

    template<typename T>
    struct LagrangeAbscisse<T, 0> {
        std::array<T, 1> poins = {0};
    };

    /**
     * @brief 1D Lagrange polynomial on a reference domain of -1 to 1
     * with evenly spaced points
     * 
     * @tparam T the floating point type
     * @tparam Pn the order of the polynomial
     * @tparam ibasis, which polynomial to get the value of
     * @param xi the point to evaluate at in the reference domain
     * @return the polynomial's value at xi
     */
    template<typename T, int Pn, int ibasis>
    T lagrange1d(const T xi){
        static_assert(ibasis <= Pn + 1);

        // special case 0th degree
        if constexpr (Pn == 0){
            return 1;
        }

        static LagrangeAbscisse<T, Pn> abscisse;
        static T denominator = []{
            T ret = 1;
            for(int i = 0; i < Pn + 1; i++){
                if(i != ibasis){
                    ret *= (abscisse.poins[ibasis] - abscisse.poins[i]);
                }
            }
            return ret;
        }();

        T numerator = 1;
        for(int i = 0; i < ibasis; i++) numerator *= (xi - abscisse.poins[i]);
        for(int i = ibasis + 1; i < Pn + 1; i++) numerator *= (xi - abscisse.poins[i]);

        return numerator / denominator;
    }

    /**
     * @brief First derivatives of 1D lagrange polyomial on
     * reference domain of [-1, 1]
     * 
     * @tparam T the floating point type
     * @tparam Pn the order of the polyomial
     * @tparam ibasis, which polynomial to get the value of
     * @param xi the point to evaluate at in the reference domain
     * @return the polyomials derivative at xi
     */
    template<typename T, int Pn, int ibasis>
    T dlagrange1d(const T xi){
        static_assert(ibasis <= Pn + 1);

        // special case 0th degree
        if constexpr (Pn == 0){
            return 0;
        }

        static LagrangeAbscisse<T, Pn> abscisse;

        T sum = 0;
        int j = ibasis;
        for(int i = 0; i < Pn + 1; i++){
            if(i != j){
                T prod = 1 / (abscisse.poins[j] - abscisse.poins[i]);
                for(int m = 0; m < Pn +1; m++){
                    if(m != i && m != j){
                        prod *= (xi - abscisse.poins[m]) / (abscisse.poins[j] - abscisse.poins[m]);
                    }
                }
                sum += prod;
            }
        }
        return sum;
    }

    // ------------------
    // - runtime ibasis -
    // ------------------
    template<typename T, int Pn>
    T lagrange1d(int ibasis, const T xi){
        // special case 0th degree
        if constexpr (Pn == 0){
            return 1;
        }

        static LagrangeAbscisse<T, Pn> abscisse;
        T denominator = [ibasis]{
            T ret = 1;
            for(int i = 0; i < Pn + 1; i++){
                if(i != ibasis){
                    ret *= (abscisse.poins[ibasis] - abscisse.poins[i]);
                }
            }
            return ret;
        }();

        T numerator = 1;
        for(int i = 0; i < ibasis; i++) numerator *= (xi - abscisse.poins[i]);
        for(int i = ibasis + 1; i < Pn + 1; i++) numerator *= (xi - abscisse.poins[i]);

        return numerator / denominator;
    }


    /**
     * @brief runtime callable derivative of lagrange basis functions
     * N_i' = N_i * \sum\limits_{m=1,m\neqi}^n \frac{1}{x - x_m}
     * 
     * Apr 23, 2020 Update: not numerically stable as xi approaches one of the abscisse
     * switching to derivative formula on wikipedia page
     * 
     * @tparam T the floating point type
     * @tparam Pn the polynomial order
     * @param ibasis the basis function number
     * @param xi the point to evalutate at
     * @return T the deriative of the ith basis function at xi
     */
    template<typename T, int Pn>
    T dlagrange1d(int ibasis, const T xi){
        // special case 0th degree
        if constexpr (Pn == 0){
            return 0;
        }

        static LagrangeAbscisse<T, Pn> abscisse;

        T sum  = 0;
        int j = ibasis;
        for(int i = 0; i < Pn + 1; i++){
            if(i != j){
                T prod = 1 / (abscisse.poins[j] - abscisse.poins[i]);
                for(int m = 0; m < Pn +1; m++){
                    if(m != i && m != j){
                        prod *= (xi - abscisse.poins[m]) / (abscisse.poins[j] - abscisse.poins[m]);
                    }
                }
                sum += prod;
            }
        }

        return sum;
    }
    
    template<typename seqT, seqT ...ints>
    void printSequence(std::integer_sequence<seqT, ints...> int_seq){
        std::cout << "sequence of size " << int_seq.size() << ": ";
        ((std::cout << ints << ' '), ...);
        std::cout << "\n";
    }

    template<typename seqT, seqT first, seqT ...ints>
    constexpr int last(){
        if constexpr(sizeof...(ints) > 0) return last<seqT, ints...>();
        else return first;
    }

    template<typename T, int Pn, typename seqT, seqT first, seqT ...ints>
    inline T dlagrangeDerivativeProd(
        int ibasis, 
        const T xi, 
        LagrangeAbscisse<T, Pn> &abscisse,
        std::integer_sequence<seqT, first, ints...> int_seq
    ){
        T term = (first == ibasis) ? 1 : (xi - abscisse.poins[first])
             / (abscisse.poins[ibasis] - abscisse.poins[first]);
        if constexpr(sizeof...(ints) > 0){
            return term * dlagrangeDerivativeProd<T, Pn>(
                ibasis, xi, abscisse,
                std::integer_sequence<seqT, ints...>{}
            );
        } else {
            return term;
        }
    }

    template<typename T, int Pn, typename seqT>
    inline T dlagrangeDerivativeProd(
        int ibasis,
        const T x,
        LagrangeAbscisse<T, Pn> &abscisse,
        std::integer_sequence<seqT> int_seq
    ) { return 1.0; }

    
    template<
        typename T,
        int Pn,
        int stop,
        typename seqT,
        seqT first,
        seqT ...ints
    >
    T dNlagrangeHelper(
        int ibasis,
        int iderivative,
        const T xi,
        std::integer_sequence<seqT, first, ints...> int_seq)
    {
        static LagrangeAbscisse<T, Pn> abscisse;
        //printSequence(int_seq);
        //std::cin.get();
        T sum = (first == ibasis) ? 0 : 1.0 
            / (abscisse.poins[ibasis] - abscisse.poins[first]);
        if (iderivative == 1){
            sum *= dlagrangeDerivativeProd<T, Pn>(
                ibasis, xi, abscisse,
                std::integer_sequence<seqT, ints...>{}
            );
        } else {
            if constexpr(sizeof...(ints) == 0){
                return 0.0;
            } else {
                sum *= dNlagrangeHelper<T, Pn, last<seqT, ints...>()>(
                    ibasis, iderivative - 1, xi,
                    std::integer_sequence<seqT, ints...>{}
                );
            }
        }
        if constexpr(first != stop){
            sum += dNlagrangeHelper<T, Pn, stop>(
                ibasis, iderivative, xi,
                std::integer_sequence<seqT, ints..., first>{}
            );
        }
        return sum;
    }

    template<typename T, int Pn>
    T dNlagrange1d(int ibasis, int iderivative, const T xi){
        return dNlagrangeHelper<T, Pn, Pn>(ibasis, iderivative, xi,
                std::make_integer_sequence<int, Pn + 1>{});
    }

    template<typename T, int Pn>
    class Lagrange1DFuncRuntime{
        public:
        inline T eval(int ibasis, T xi){ return lagrange1d<T, Pn>(ibasis, xi); }

        inline T derivative(int ibasis, T xi){ return dlagrange1d<T, Pn>(ibasis, xi); }

        inline T derivative2nd(int ibasis, T xi){ return jthDerivative(ibasis, 2, xi); }

        inline T jthDerivative(int ibasis, int jderivative, T xi){ return dNlagrange1d<T, Pn>(ibasis, jderivative, xi); }

        inline void derivativeOperator();
    };
}

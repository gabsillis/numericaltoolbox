/**
 * @file ODE.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief ODE definitions
 * @version 0.1
 * @date 2022-01-30
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <functional>
namespace MATH {
    namespace ODE {

        /**
         * @brief Dense vector function evaluation calulates d(x_i, t)/ dt
         * @tparam T the floating point type
         * std::function arguments:
         * [in] the input vector x
         * [in] the time t
         * [out] \frac{dx_i}{dt}
         */
        template<typename T>
        using ode_func = std::function<void(T *, T, T*)>;
    }
}